import {AppRegistry, LogBox} from 'react-native';
import React from 'react';
import App from './App';
import {name as appName} from './app.json';
import {Provider} from 'react-redux';
import {legacy_createStore, applyMiddleware, compose} from 'redux';
import reducer from './src/redux/reducers';

// LogBox.ignoreLogs(['Remote debugger']);
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const logger = store => next => action => {
  const result = next(action);
  return result;
};

const store = legacy_createStore(
  reducer,
  composeEnhancers(applyMiddleware(logger)),
);

const RNRedux = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

AppRegistry.registerComponent(appName, () => RNRedux);
