import deviceInfoModule from 'react-native-device-info';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { TOKEN } from '../utils/api';
import moment from 'moment';
import { postTokenlessRequest } from '../network';
import { homeRoot } from '../utils/setRoot';
export default async function TokenUpdate({Id,navigation}) {
    const uniqueId = deviceInfoModule.getUniqueId();
    const token = await AsyncStorage.getItem('token');
    const Phone = await AsyncStorage.getItem('phone');
    const url = TOKEN;
    const data = {
        id: Phone,
        customer_id: Id,
        device_id: uniqueId,
        FCM_token: token,
        created_date: moment().format('MMMM Do YYYY, h:mm:ss a'),
        created_by: Id
    };
    const res = await postTokenlessRequest(url, data);
    if (res !== null) {
        if (res.StatusCode === 200) {
           
        }
    }
}
