import {  homeRoot, rootPush } from "../utils/setRoot";
import TokenUpdate from "./token";

export default function SplashNavigate(walkthrough, login, id) {
    if (walkthrough) {
        if (login) {
            TokenUpdate(id)
        } else {
            homeRoot()
        }
    } else {
        rootPush('walkThrough');
    }
}
