export const BASE_URL = 'https://api.kharpantrade.com/';
export const LOG_IN = 'login/authenticate';
export const HOME_URL = BASE_URL + 'home/';
export const GET_DATA = HOME_URL + 'getproductlistall';
export const CATEGORY = 'getcategory';

export const ORDER_URL = BASE_URL + 'order/getorderlist/';
export const ORDER_DETAILS = BASE_URL + 'order/GetOrderDetailList/';
export const CHECK_OUT = BASE_URL + 'order/Checkout';

export const GET_PROFILE = BASE_URL + 'profile/GetProfileInfo/';
export const GET_CONTACT = BASE_URL + 'profile/GetContactInfo';
export const CHNAGE_PASSWORD = BASE_URL + 'profile/ChangePassword';
export const PAYMENT_HISTORY = BASE_URL + 'profile/getpaymentinfo';

export const CHNAGE_PRODUTCT_PREF = 'profile/changeproductpreference';
export const FORGETPASSWORD = 'profile/forgetpassword?contactnumber=';
export const TOKEN = BASE_URL + 'login/insertFCMToken';
export const REGISTER = BASE_URL + 'registration/registrationrequest';
