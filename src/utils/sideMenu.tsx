import React, {useState, useEffect} from 'react';
import {
  TextInput,
  View,
  FlatList,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import AppSettings from './AppSettings';
import FastImage from 'react-native-fast-image';
import {CenterView} from '../component/Widgets';
import Text from '../component/text';

import {GET_PROFILE} from './api';
import {getRequests} from '../network';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useSelector} from 'react-redux';
const SideMenu = ({navigation}) => {
  const sucess = useSelector(state => state.log.success);
  const [customerCode, setcustomerCode] = useState('');
  const getCustomerCode = async () => {
    const code = await AsyncStorage.getItem('customerCode');
    setcustomerCode(code);
  };
  const [Menu, setMenu] = useState([
    {name: 'Home', icon: 'home', navigate: 'home'},
    {
      name: 'My Orders',
      icon: 'list-alt',
      navigate: 'myOrder',
    },
    {
      name: 'Profile',
      icon: 'user-tie',
      navigate: 'profile',
    },
    {name: 'Setting', icon: 'cogs', navigate: 'setting'},
    // {name: 'Help and Support', icon: 'info', navigate: 'setting'},
    {name: 'Contact', icon: 'address-book', navigate: 'contactUs'},
    {name: 'Show Walkthrough', icon: 'walking', navigate: 'walkthrough'},
  ]);
  const [userData, setuserData] = useState([]);

  useEffect(() => {
    if (sucess) {
      getProfile();
      getCustomerCode();
    }
  }, [sucess]);

  const getProfile = async () => {
    const id = await AsyncStorage.getItem('userID');
    const url = GET_PROFILE + id;
    const res = await getRequests(url);
    console.log(res);
    if (res) {
      setuserData(res);
      if (res.product_type) {
        const data = res.product_type.toString();
        AsyncStorage.setItem('productType', data);
        // console.log(res.product_type)
      }
    } else {
      console.log('error');
    }
  };
  const navigate = item => {
    if (!sucess && (item === 'myOrder' || item === 'profile')) {
      navigation.navigate('SignIn');
    } else {
      navigation.navigate(item);
    }
    // navigation.navigate(item);
  };

  return (
    <View style={styles.main}>
      <CenterView innerStyle={styles.profile}>
        <CenterView
          innerStyle={{
            flex: 0,
            width: 100,
            height: 100,
            borderRadius: 50,
            backgroundColor: '#9AA1B1',
          }}>
          <Text style={{fontSize: 50, fontWeight: '600', color: '#ffffff'}}>
            {' '}
            {sucess ? customerCode : 'G'}
          </Text>
        </CenterView>
        <Text style={styles.heading}>{userData?.customer_name}</Text>
        <Text style={styles.text}>{userData?.contactno}</Text>
      </CenterView>
      <View style={styles.line} />
      <FlatList
        data={Menu}
        keyExtractor={(item, index) => item.name}
        showsVerticalScrollIndicator={false}
        renderItem={({item}) => {
          return (
            <TouchableOpacity onPress={() => navigate(item.navigate)}>
              <View style={styles.item}>
                <Icon
                  name={item.icon}
                  size={20}
                  color={AppSettings.iconColor}
                  style={{width: 35}}></Icon>
                <Text style={styles.text}>{item.name}</Text>
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};

export default SideMenu;
const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: AppSettings.lightBackGround,
  },
  profile: {
    flex: 0,
    padding: 20,
    marginVertical: 20,
  },
  heading: {
    fontSize: 20,
    fontWeight: '700',
  },
  text: {
    fontSize: 16,
    color: AppSettings.primaryfontColor,
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
    padding: 25,
  },
  line: {
    height: 3,
    marginBottom: 5,
    backgroundColor: AppSettings.itembackGround,
  },
});
