import Snackbar from 'react-native-snackbar';

export function SnackMsg(msg) {
  Snackbar.show({
    text: msg,
    duration: Snackbar.LENGTH_SHORT,
  });
}
