import React from 'react';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {Navigation} from 'react-native-navigation';
import {persistStore, persistReducer} from 'redux-persist';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import {PersistGate} from 'redux-persist/lib/integration/react';
import FSStorage from 'redux-persist-fs-storage';
import reducers from '../redux/reducers';
import {gestureHandlerRootHOC} from 'react-native-gesture-handler';
import SplashScreen from '../screens/splash';
import WalkThroughScreen from '../screens/walkThrough';
import HomeScreen from '../screens/home';
import SignInScreen from '../screens/signIn';
import SignUpScreen from '../screens/signUp';
import forgetPassword from '../screens/forgetPassword';
import ProfileScreen from '../screens/profile';
import CartScreen from '../screens/cart';
import ProductScreen from '../screens/productDetails';
import OrderScreen from '../screens/order';
import SearchScreen from '../screens/search';
import ProductDetailsScreen from '../screens/productDetails';
import OrderDetailsScreen from '../screens/orderDetails';
import VegScreen from '../screens/vegNonveg';
import delivryScreen from '../screens/delivry';
import passwordChangeScreen from '../screens/changePassword';
import PaymentScreen from '../screens/payment';
import ContactScreen from '../screens/contact';
import SideMenuScreen from '../utils/sideMenu';
import SettingScreen from '../screens/setting';
import RejectedScreen from '../screens/Order/Rejected';
import App from '../../App';


const middleware = [thunk];

const persistConfig = {
  key: 'root',
  storage: FSStorage(),
  keyPrefix: '',
  timeout: 0,
  stateReconciler: autoMergeLevel2,
  blacklist: ['kharpan'],
};

const preducer = persistReducer(persistConfig, reducers);
// const store = createStore(reducers, applyMiddleware(...middleware));
const store = createStore(preducer, applyMiddleware(...middleware));
const persistor = persistStore(store);

function ReduxProvider(Component) {
  return (props) => (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <Component {...props} />
      </PersistGate>
    </Provider>
  );
}

export function registerScreen() {
  Navigation.registerComponent(
    'AppScreen',
    () => ReduxProvider(gestureHandlerRootHOC(App)),
    () => App,
  );
  Navigation.registerComponent(
    'splash',
    () => ReduxProvider(gestureHandlerRootHOC(SplashScreen)),
    () => SplashScreen,
  );
  Navigation.registerComponent(
    'walkThrough',
    () => ReduxProvider(gestureHandlerRootHOC(WalkThroughScreen)),
    () => WalkThroughScreen,
  );
  Navigation.registerComponent(
    'home',
    () => ReduxProvider(gestureHandlerRootHOC(HomeScreen)),
    () => HomeScreen,
  );
  Navigation.registerComponent(
    'signIn',
    () => ReduxProvider(gestureHandlerRootHOC(SignInScreen)),
    () => SignInScreen,
  );
  Navigation.registerComponent(
    'forgetPassword',
    () => ReduxProvider(gestureHandlerRootHOC(forgetPassword)),
    () => forgetPassword,
  );
  Navigation.registerComponent(
    'profile',
    () => ReduxProvider(gestureHandlerRootHOC(ProfileScreen)),
    () => ProfileScreen,
  );
  Navigation.registerComponent(
    'cart',
    () => ReduxProvider(gestureHandlerRootHOC(CartScreen)),
    () => CartScreen,
  );
  Navigation.registerComponent(
    'product',
    () => ReduxProvider(gestureHandlerRootHOC(ProductScreen)),
    () => ProductScreen,
  );
  Navigation.registerComponent(
    'order',
    () => ReduxProvider(gestureHandlerRootHOC(OrderScreen)),
    () => OrderScreen,
  );
  Navigation.registerComponent(
    'search',
    () => ReduxProvider(gestureHandlerRootHOC(SearchScreen)),
    () => SearchScreen,
  );
  Navigation.registerComponent(
    'productDetails',
    () => ReduxProvider(gestureHandlerRootHOC(ProductDetailsScreen)),
    () => ProductDetailsScreen,
  );
  Navigation.registerComponent(
    'orderDetails',
    () => ReduxProvider(gestureHandlerRootHOC(OrderDetailsScreen)),
    () => OrderDetailsScreen,
  );
  Navigation.registerComponent(
    'vegnon',
    () => ReduxProvider(gestureHandlerRootHOC(VegScreen)),
    () => VegScreen,
  );
  Navigation.registerComponent(
    'delivery',
    () => ReduxProvider(gestureHandlerRootHOC(delivryScreen)),
    () => delivryScreen,
  );
  Navigation.registerComponent(
    'changePassword',
    () => ReduxProvider(gestureHandlerRootHOC(passwordChangeScreen)),
    () => passwordChangeScreen,
  );
  Navigation.registerComponent(
    'payment',
    () => ReduxProvider(gestureHandlerRootHOC(PaymentScreen)),
    () => PaymentScreen,
  );
  Navigation.registerComponent(
    'contact',
    () => ReduxProvider(gestureHandlerRootHOC(ContactScreen)),
    () => ContactScreen,
  );
  Navigation.registerComponent(
    'menu',
    () => ReduxProvider(gestureHandlerRootHOC(SideMenuScreen)),
    () => SideMenuScreen,
  );
  Navigation.registerComponent(
    'setting',
    () => ReduxProvider(gestureHandlerRootHOC(SettingScreen)),
    () => SettingScreen,
  );
  Navigation.registerComponent(
    'rejected',
    () => ReduxProvider(gestureHandlerRootHOC(RejectedScreen)),
    () => RejectedScreen,
  );
  Navigation.registerComponent(
    'signUp',
    () => ReduxProvider(gestureHandlerRootHOC(SignUpScreen)),
    () => SignUpScreen,
  );
}
