import React, {useEffect} from 'react';
import {
  View,
  Text,
  TouchableNativeFeedbackComponent,
  TouchableOpacity,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {CenterView} from './Widgets';
import {useDispatch, useSelector} from 'react-redux';
import {closeModel} from '../redux/action/colorAction';
export default function ColorCode() {
  const url = useSelector(state => state.color.url);
  const dispatch = useDispatch();
  return (
    <CenterView
      innerStyle={{
        flex: 1,
        marginTop: 50,
        backgroundColor: 'rgba(192,192,192, 0.9)',
      }}>
      <TouchableOpacity
        onPress={() => {
          dispatch(closeModel());
        }}
        style={{marginLeft: 305, marginBottom: 5}}>
        <Icon name="times-circle" size={20} color="red" />
      </TouchableOpacity>
      <FastImage
        style={{height: 400, width: 300}}
        source={{
          uri: url,
          headers: {Authorization: 'someAuthToken'},
          priority: FastImage.priority.normal,
        }}
        resizeMode={FastImage.resizeMode.cover}
      />
    </CenterView>
  );
}
