import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {CenterView, Row} from './Widgets';

import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';
import Icon from 'react-native-vector-icons/AntDesign';
import {useDispatch, useSelector} from 'react-redux';
import {getRequests} from '../network';
import {
  notificationNotSet,
  notificationSet,
  remainingDays,
} from '../redux/action/notificationAction';
import {GET_PROFILE, HOME_URL} from '../utils/api';
import {useFocusEffect} from '@react-navigation/native';
const Header = props => {
  const Name = useSelector(state => state.walkthrough.customerCode);
  const Notification = useSelector(state => state.notification);
  const [deilveryRemaining, setdeilveryRemaining] = useState('');
  const [nextDelivery, setnextDelivery] = useState([]);
  const DateArray = useSelector(state => state.day.list);
  const Id = useSelector(state => state.walkthrough.userID);
  const sucess = useSelector(state => state.log.success);
  const dispatch = useDispatch();
  const [customerCode, setcustomerCode] = useState('');

  useFocusEffect(
    React.useCallback(() => {
      getCustomerCode();
      NextDeliveryData();
    }, [props.navigation]),
  );

  const getCustomerCode = async () => {
    const id = await AsyncStorage.getItem('userID');
    const url = GET_PROFILE + id;
    const res = await getRequests(url);

    if (res) {
      setcustomerCode(res.customer_code);
    } else {
      console.log('error customer code');
    }
  };

  const NextDeliveryData = async () => {
    const Id = await AsyncStorage.getItem('userID');
    const url = HOME_URL + 'gethomedata/' + Id + "/1/0/'";
    const res = await getRequests(url);
    if (res) {
      const data = res.NextDeliveryData;
      data.map(item => {
        setnextDelivery(item);
      });
    }
  };
  const getNotification = async () => {
    var today;
    var thisWeek = true;

    var day = moment(new Date()).format('dddd').toLowerCase();
    if (day == 'sunday') {
      today = 1;
    } else if (day == 'monday') {
      today = 2;
    } else if (day === 'tuesday') {
      today = 3;
    } else if (day === 'wednesday') {
      today = 4;
    } else if (day === 'thursday') {
      today = 5;
    } else if (day === 'friday') {
      today = 6;
    } else {
      today = 7;
    }

    var lenthg = DateArray.length;

    for (var i = 0; i < DateArray.length; i++) {
      if (thisWeek) {
        if (today == DateArray[i]) {
          setdeilveryRemaining(0);
          if (Notification.scheduled == false) {
            // scheduledNotification();
            dispatch(notificationSet());
          }
          break;
        } else if (today < DateArray[i]) {
          // console.log(" notification after ", DateArray[i] - today, "day")
          setdeilveryRemaining(DateArray[i] - today);
          dispatch(remainingDays(DateArray[i] - today - 1));
          dispatch(notificationNotSet());
          break;
        }
        if (i == DateArray.length - 1) {
          i = -1;
          thisWeek = false;
        }
      } else {
        if (today > DateArray[i]) {
          setdeilveryRemaining(DateArray[i] - today + 7);
          dispatch(remainingDays(DateArray[i] - today + 7 - 1));
          dispatch(notificationNotSet());
          break;
        }
      }
    }
  };

  const sideMenu = () => {
    props.navigation.openDrawer();
  };

  const goBack = () => {
    props.navigation.goBack();
  };

  return (
    <View style={[styles.header, props.style]}>
      {/*---------------header left------------------ */}
      <Row innerStyle={{alignSelf: 'flex-start'}}>
        {props.sidemenu ? (
          <TouchableOpacity onPress={sideMenu}>
            <FastImage
              resizeMode={FastImage.resizeMode.contain}
              style={{width: 20, height: 40}}
              source={require('../../src/icons/Menu.png')}
            />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity onPress={() => goBack()}>
            <FastImage
              resizeMode={FastImage.resizeMode.contain}
              style={{width: 20, height: 40, resizeMode: 'center'}}
              source={require('../../src/icons/left-back.png')}
            />
          </TouchableOpacity>
        )}

        <View style={{paddingTop: 5}}>
          <Text
            style={{
              fontFamily: 'Debby',
              marginLeft: 15,
              fontSize: 23,
            }}>
            {props.heading}
          </Text>
          <FastImage
            resizeMode={FastImage.resizeMode.contain}
            style={{flex: 0.5, marginTop: -3}}
            source={require('../../src/icons/headerDown.png')}
          />
        </View>
      </Row>
      {/*---------------header right------------------ */}

      {/*---------------header right------------------ */}
      <Row innerStyle={{alignItems: 'center', justifyContent: 'space-between'}}>
        {sucess ? (
          props.delivery ? (
            <Text
              style={{
                fontSize: 12,
                marginRight: 8,
                backgroundColor: '#FFA500',
                padding: 4,
                borderRadius: 8,
                textAlign: 'center',
                color: '#fff',
                paddingHorizontal: 8,
              }}>
              {nextDelivery.pending_order_count
                ? nextDelivery.pending_order_count == 0
                  ? 'No pending order'
                  : 'You have ' +
                    nextDelivery.pending_order_count +
                    ' pending orders'
                : 'No pending order'}
            </Text>
          ) : null
        ) : null}

        {props.rejected ? (
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate('rejected');
            }}>
            <Icon
              name="closecircleo"
              size={20}
              color="red"
              style={{marginRight: 8}}
            />
          </TouchableOpacity>
        ) : null}
        {props.profile ? (
          <TouchableOpacity
            onPress={() =>
              sucess ? props.navigation.navigate('profile') : null
            }>
            <CenterView
              innerStyle={{
                flex: 0,
                width: 36,
                height: 36,
                borderRadius: 18,
                backgroundColor: '#9AA1B1',
              }}>
              <Text style={{fontSize: 18, fontWeight: '600', color: '#ffffff'}}>
                {sucess ? customerCode : 'G'}
              </Text>
            </CenterView>
          </TouchableOpacity>
        ) : null}
      </Row>
    </View>
  );
};

export default Header;
const styles = StyleSheet.create({
  header: {
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 8,
    // backgroundColor:"red"
  },
});
