import React from 'react';
import { Text, StyleSheet, Dimensions, View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import { useDispatch } from 'react-redux';
import { closeModel } from '../redux/action/colorAction';
import AppSettings from '../utils/AppSettings';
import { homeRoot } from '../utils/setRoot';
import { CenterView } from './Widgets';
const Congrats = (props) => {
  return (
    <View style={styles.main}>
      <View style={styles.modalbody}>
        <View style={styles.circle}>
          <Icon name={'check'} size={50} color="#04bc6c" />
        </View>
        <Text style={styles.text}>{props.title}</Text>
        <View style={styles.orderbox}>
          <Text style={styles.texttitle}>Order ID  :</Text>
          <Text style={styles.textgreen}>{' ' + props.orderid}</Text>
        </View>
      </View>
    </View>
  );
};
const AfterRegister = (props) => {
  const dispatch = useDispatch();
  return (
    <View style={styles.main}>
      <View style={styles.modalbody}>
        <View style={styles.circle}>
          <Icon name={'check'} size={50} color="#04bc6c" />
        </View>
        <Text style={styles.text}>{props.title}</Text>
        <TouchableOpacity onPress={() => {
          homeRoot();
          dispatch(closeModel());
        }}
          style={styles.okBotton}>
          <CenterView>
            <Text style={{ fontSize: 20 }}>OK</Text>
          </CenterView>

        </TouchableOpacity>
        {/* <View style={styles.orderbox}>
          <Text style={styles.texttitle}>Order ID  :</Text>
        </View> */}
      </View>
    </View>
  );
};
export { Congrats, AfterRegister };
const styles = StyleSheet.create({
  main: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.9)',
  },
  modalbody: {
    backgroundColor: '#F9FAFF',
    elevation: 1,
    borderRadius: 20,
    justifyContent: 'center',
    padding: 30,
    alignItems: 'center',
    marginHorizontal: 20,
  },
  circle: {
    height: 100,
    width: 100,
    borderRadius: 150,
    backgroundColor: AppSettings.white,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 1
  },
  orderbox: {
    marginVertical: 10,
    backgroundColor: AppSettings.white,
    padding: 5,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    fontSize: AppSettings.maxfontsize,
    color: '#04bc6c',
    paddingTop: 20,
    textAlign: "center"
  },
  texttitle: {
    fontSize: AppSettings.secondaryfontsize,
    color: AppSettings.h2,
  },
  textgreen: {
    fontSize: AppSettings.secondaryfontsize,
    color: '#04bc6c',
  },
  okBotton: {
    height: 40,
    width: 80,
    borderRadius: 10,
    alignSelf: 'flex-end',
    marginTop: 20,
    borderColor: "grey",
    borderWidth: 0.2,
    // backgroundColor:"#fafafa"
  }
});

