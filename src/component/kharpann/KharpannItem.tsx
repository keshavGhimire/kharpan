import React, {useEffect, useMemo, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ImageBackground,
  Modal,
  Alert,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {BorderlessButton} from 'react-native-gesture-handler';
import AppSettings from '../../utils/AppSettings';
import Icon from 'react-native-vector-icons/Entypo';
import {incrementList, decrementList} from '../../redux/action/listAction';
import {CenterView, Row} from '../Widgets';

import {imageUrl, openModel} from '../../redux/action/colorAction';
import AsyncStorage from '@react-native-async-storage/async-storage';
const KharpannItem = ({item, navigation, sucess}) => {
  const dispatch = useDispatch();

  const Navigate = data => {
    navigation.navigate('productDetails', {name: data});
  };
  return useMemo(() => {
    return (
      <View style={styles.item}>
        <View>
          {item.quantity > 0.001 ? (
            <View style={{flex: 1, alignItems: 'flex-end'}}>
              <ImageBackground
                style={{
                  width: 40,
                  height: 40,
                  right: -1,
                  paddingTop: 7,
                  paddingLeft: 12,
                }}
                resizeMode="contain"
                source={require('../../icons/cart.png')}>
                <FastImage
                  style={{
                    width: 20,
                    height: 20,
                  }}
                  resizeMode="contain"
                  source={require('../../icons/shopping-cart.png')}
                />
              </ImageBackground>
            </View>
          ) : null}

          <TouchableOpacity onPress={() => Navigate(item)}>
            <Row innerStyle={{alignItems: 'center'}}>
              <FastImage
                style={{
                  width: 80,
                  height: 60,
                  resizeMode: 'cover',
                  marginRight: 15,
                  marginLeft: 10,
                }}
                source={{
                  uri: item.image_url,
                }}
              />
              <View style={{paddingTop: 20}}>
                <Text style={styles.heading}>{item.product_name}</Text>

                <Text style={styles.price}>
                  {item.rate} /{item.qty}
                </Text>
              </View>
            </Row>
          </TouchableOpacity>
          <View style={styles.line} />
          <Row
            innerStyle={{
              justifyContent: 'space-between',
              margin: 10,
              marginHorizontal: 20,
            }}>
            <Row innerStyle={{paddingTop: 6}}>
              <Text style={[styles.heading, {width: 93}]}>
                {'Rs. ' + (item.original_rate * item.quantity).toFixed(2)}
              </Text>
              <View style={styles.farmerMarket}>
                <Text style={{color: '#fff', fontSize: 12}}>
                  {item.product_source_id === 1 ? 'Farmer' : 'Market'}
                </Text>
              </View>
              <TouchableOpacity
                onPress={() => {
                  // dispatch(openModel());
                  // dispatch(imageUrl(item.product_color_info));
                }}>
                <CenterView innerStyle={styles.outCircle}>
                  <View
                    style={[
                      styles.circle,
                      {backgroundColor: item.product_color_code},
                    ]}>
                    <Icon name="info-with-circle" size={8} color="#BEBBBB" />
                  </View>
                </CenterView>
              </TouchableOpacity>
            </Row>
            <Row
              innerStyle={{
                width: 90,
                justifyContent: 'space-around',
                right: -10,
              }}>
              <TouchableOpacity
                onPress={() => {
                  if (item.quantity > 0) {
                    dispatch(decrementList(item.id));
                  }
                }}>
                {/* <View style={styles.itembtn}>
                  <View style={[styles.innerCircle, {}]}>
                    <Text
                      style={[
                        styles.btnPress,
                        { fontSize: 26, color: '#ec9488' },
                      ]}>
                      -
                    </Text>
                  </View>
                </View> */}
                <CenterView innerStyle={styles.itembtn}>
                  <Text
                    style={[
                      styles.btnPress,
                      {color: '#ec9488', fontSize: 24, fontWeight: 'bold'},
                    ]}>
                    -
                  </Text>
                </CenterView>
              </TouchableOpacity>
              <Text
                style={[styles.btnText, {paddingTop: 5, marginHorizontal: 15}]}>
                {item.quantity.toFixed(2)}
              </Text>
              <TouchableOpacity
                onPress={async () => {
                  const Id = await AsyncStorage.getItem('userID');
                  
                  if (Id) {
                    dispatch(incrementList(item.id));
                  } else {
                    navigation.navigate('SignIn');
                  }
                }}>
                {/* <View style={styles.itembtn}>
                  <View style={[styles.innerCircle]}>
                    <Text
                      style={[
                        styles.btnPress,
                        { color: AppSettings.signinbutton },
                      ]}>
                      +
                    </Text>
                  </View>
                </View> */}
                <CenterView innerStyle={styles.itembtn}>
                  <Text
                    style={[
                      styles.btnPress,
                      {color: AppSettings.signinbutton},
                    ]}>
                    +
                  </Text>
                </CenterView>
              </TouchableOpacity>
            </Row>
          </Row>
        </View>
      </View>
    );
  }, [item.quantity]);
};
const OrderCard = props => {
  const [item, setitem] = useState([]);
  useEffect(() => {
    setitem(props.item);
  }, []);
  return (
    <View style={styles.item}>
      <View>
        <View style={{flex: 1, alignItems: 'flex-end'}}>
          <ImageBackground
            style={{
              width: 40,
              height: 40,
              resizeMode: 'cover',
              right: -1,
              paddingTop: 7,
              paddingLeft: 12,
            }}
            source={require('../../icons/cart.png')}>
            <FastImage
              style={{
                width: 20,
                height: 20,
                resizeMode: 'center',
              }}
              source={require('../../icons/shopping-cart.png')}
            />
          </ImageBackground>
        </View>
        <Row innerStyle={{alignItems: 'center'}}>
          <FastImage
            style={{
              width: 80,
              height: 50,
              resizeMode: 'cover',
              marginRight: 5,
            }}
            source={{
              uri: item.image_url,
            }}
          />
          <View style={{paddingTop: 20}}>
            <Text style={styles.heading}>{item.product_name}</Text>

            <Text style={styles.price}>
              Rs.{item.rate} / {item.unit_code}
            </Text>
          </View>
        </Row>
        <View style={styles.line} />
        <Row
          innerStyle={{
            justifyContent: 'space-between',
            margin: 10,
            marginHorizontal: 20,
          }}>
          <Row>
            <Text style={[styles.heading, {width: 65}]}>Rs. {item.total}</Text>
            <View style={styles.farmerMarket}>
              <Text style={{color: '#fff', fontWeight: '700'}}>
                {item.product_source}
              </Text>
            </View>
            <View style={styles.circle}>
              <Icon name="info-with-circle" size={8} color="#BEBBBB" />
            </View>
          </Row>

          <Text
            style={[
              styles.btnText,
              {fontSize: 16, paddingTop: 2, marginLeft: 3},
            ]}>
            {item.quantity}
          </Text>
        </Row>
      </View>
    </View>
  );
};
export {KharpannItem, OrderCard};

const styles = StyleSheet.create({
  item: {
    height: 145,
    width: '98%',
    backgroundColor: '#ffffff',
    elevation: 3,
    marginBottom: 18,
    borderRadius: 8,
    alignSelf: 'center',
    zIndex: 0,
    // borderWidth: 0.4,
    // borderColor: '#DFD9D9',
  },
  line: {
    height: 0.5,
    width: '90%',
    backgroundColor: '#C1BDBD',
    alignSelf: 'center',
    marginTop: 8,
  },
  farmerMarket: {
    height: 18,
    width: 50,
    borderRadius: 3,
    backgroundColor: AppSettings.SecondaryText,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
  },
  itembtn: {
    flex: 0,
    height: 30,
    width: 30,
    borderRadius: 15,
    backgroundColor: '#F5F5F5',
    // backgroundColor: 'red',
  },
  innerCircle: {
    flex: 0,
    height: 26,
    width: 26,
    borderColor: '#B4B4B4',
    borderRadius: 13,
    borderWidth: 1.6,
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerCircles: {
    height: 22,
    width: 22,
    borderRadius: 11,
    backgroundColor: '#fafafa',
    borderWidth: 1,
    borderColor: '#B4B4B4',
  },
  heading: {
    height: 40,
    width: 200,
    fontSize: 15,
    fontWeight: '700',
  },
  price: {
    fontSize: AppSettings.secondaryfontsize,
    color: AppSettings.SecondaryText,
    fontWeight: '700',
    paddingTop: 5,
  },
  btnText: {
    fontSize: 18,
    color: '#B8B8B8',
  },
  btnPress: {
    fontSize: 22,
    color: '#B8B8B8',
    top: -2,
  },
  circle: {
    height: 15,
    width: 15,
    borderRadius: 7.5,
    alignItems: 'flex-end',
  },
  outCircle: {
    flex: 0,
    height: 30,
    width: 30,
    top: -5,
    marginLeft: 12,
  },
});
