import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import Background from './background';

import {hideVegNon} from '../redux/action/walkthroughAction';
import Text from './text';
import AppSettings from '../utils/AppSettings';
import {CustomButton} from './button';
import {RadioButton} from 'react-native-paper';
import {useDispatch, useSelector} from 'react-redux';
import {BASE_URL, CHNAGE_PRODUTCT_PREF} from '../utils/api';
import {postTokenlessRequest} from '../network';

const VegNonVeg = () => {
  const dispatch = useDispatch();
  const [checked, setChecked] = useState('1');
  const [loading, setloading] = useState(false);
  const Id = useSelector(state => state.walkthrough.userID);
  const onSubmit = async () => {
    const url = BASE_URL + CHNAGE_PRODUTCT_PREF;
    const data = {
      customer_id: Id,
      product_type: parseInt(checked),
    };

    const res = await postTokenlessRequest(url, data);
    if (res !== null) {
      if (res.StatusCode === 200) {
        setloading(false);
        dispatch(hideVegNon());
      }
    }
  };
  return (
    <Background style={{backgroundColor: '#ffffff'}}>
      <View style={{flex: 5}}>
        <Text style={styles.heading}>Wait ! Just a second.</Text>

        <View style={{marginVertical: 20}} />
        <View style={styles.checkboxContainer}>
          <Text style={styles.label}>I am vegetarian but I eat egg</Text>
          <RadioButton
            value="3"
            status={checked === '3' ? 'checked' : 'unchecked'}
            color="green"
            onPress={() => setChecked('3')}
          />
        </View>
        <View style={styles.checkboxContainer}>
          <Text style={styles.label}>I am non vegetarian</Text>
          <RadioButton
            value="2"
            color={AppSettings.signinbutton}
            status={checked === '2' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('2')}
          />
        </View>
        <View style={styles.checkboxContainer}>
          <Text style={styles.label}>I am vegetarian</Text>
          <RadioButton
            value="1"
            color={AppSettings.signinbutton}
            status={checked === '1' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('1')}
          />
        </View>
      </View>
      <View style={{flex: 1, alignItems: 'center'}}>
        <CustomButton onPress={onSubmit} loading={loading}>
          Save and proceed.
        </CustomButton>
      </View>
    </Background>
  );
};

const styles = StyleSheet.create({
  checkboxContainer: {
    height: 45,
    width: '98%',
    alignSelf: 'center',
    backgroundColor: '#E9E8E8',

    flexDirection: 'row',
    marginBottom: 20,
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  checkbox: {
    alignSelf: 'center',
  },
  label: {
    margin: 12,
    textAlign: 'left',
    color: AppSettings.SecondaryText,
    fontWeight: '700',
  },
  heading: {
    fontSize: 24,
    marginTop: '20%',
    fontWeight: '700',
  },
});

export default VegNonVeg;
