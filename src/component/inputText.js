import React, { useState } from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';
import FastImage from 'react-native-fast-image';
import { Row } from './Widgets';
import Icon from 'react-native-vector-icons/FontAwesome5';

const CustomInput = (props) => {
  const [visible, setVisible] = useState(true);
  return (
    <View style={styles.box} >
      <Row innerStyle={{ alignItems: 'center', }}>
        <FastImage
          resizeMode={FastImage.resizeMode.contain}
          style={{ width: 20, height: 45 }}
          source={props.image}
        />
        <TextInput
          {...props}
          placeholder={props.placeholder}
          placeholderTextColor={'#6C6C6C'}
          style={styles.textinput}
          secureTextEntry={props.show ? visible : false}
          value={props.value}
          onChangeText={props.onChangeText}
        />
      </Row>
      {props.show ? (
        <Icon
          name={visible ? "eye" : "eye-slash"}
          size={16}
          color="#6C6C6C"
          onPress={() => setVisible(!visible)}
        />
      ) : null}
    </View>
  );
};
const SignInInput = (props) => {
  const [visible, setVisible] = useState(true);
  return (
    <View style={styles.input} >

      <TextInput
        {...props}
        placeholder={props.placeholder}
        placeholderTextColor={'#6C6C6C'}
        style={styles.textinput1}
        secureTextEntry={props.show ? visible : false}
        value={props.value}
        onChangeText={props.onChangeText}
      />
      {props.show ? (
        <Icon
          name={visible ? "eye" : "eye-slash"}
          size={16}
          color="#6C6C6C"
          onPress={() => setVisible(!visible)}
        />
      ) : null}
    </View>
  );
};

export { CustomInput, SignInInput };
const styles = StyleSheet.create({
  box: {
    height: 50,
    alignItems: 'center',
    borderRadius: 10,
    flexDirection: 'row',
    marginVertical: 3,
    paddingHorizontal: 20,
    backgroundColor: '#EEEEEE',

    justifyContent: 'space-between',
  },
  textinput: {
    color: '#000000',
    paddingLeft: 20,
    fontSize: 16,
    fontWeight: '400',
    
  },
  input: {
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 3,
    justifyContent: "space-between",
    borderBottomColor: "#000000",
    borderBottomWidth:2
  },
  textinput1: {
    color: '#000000',
    fontSize: 18,
    fontWeight: '400',
    width: "80%",
    paddingLeft: 3,
  },
});
