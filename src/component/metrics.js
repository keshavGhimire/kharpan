import {Dimensions,Platform,PixelRatio} from 'react-native'


//DEVICE HEIGHT AND WIDTH
const HEIGHT = Dimensions.get('window').height
const WIDTH = Dimensions.get('window').width




// based on iphone 5s's scale
const scale = WIDTH / 320;

let normalize=(size)=> {
  const newSize = size * scale 
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(newSize))
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
  }
}



export {
    HEIGHT,
    WIDTH,
    normalize
};