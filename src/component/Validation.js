import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { signUp, logIn, forgot, resetPsw } from '../Network/PostServer';


    class SignupForm extends Component {
        constructor(props){
            super(props);
            this.props.onRef(this)
        }
        _checkSigninValidation = (username,email,password,confirmPassword) => {
            
            if(username != null && email != null && password != null && confirmPassword != null ){
                if(username.length > 0 && password.length > 0 && email.length > 0 && confirmPassword.length > 0){
                   if(confirmPassword == password){
                        this.props.onLogingStateResponse(true)
                        this._signupCall(username,email,password,confirmPassword)
                   }else{
                       this.props.onConfirmPasswordResponse(true)
                   }
                }else{
                    if(username.length == 0 ){
                        this.props.onUserResponse(true)
                    }
                     if(email.length == 0){
                        this.props.onEmailResponse(true)
                    } 
                    if(password.length == 0){
                        this.props.onPasswordResponse(true)
                    }
                    if(confirmPassword.length == 0){
                        this.props.onConfirmResponse(true)
                    }
                }
                
            }else{
                if(username == null && email == null && password == null && confirmPassword == null){
                    this.props.onErrorResponse(true,true,true,true)
                
                }else{

                    if(username == null ){
                    
                        this.props.onUserResponse(true)
                    }
                     if(email == null){
                        this.props.onEmailResponse(true)
                    } 
                    if(password == null){
                        this.props.onPasswordResponse(true)
                    }
                    if(confirmPassword == null){
                        this.props.onConfirmResponse(true)
                    }
                }
 
            }
        }
        _signupCall = (username,email,password,confirmPassword) => {
            try {
                const data = {
                    "name": username,
                    "email": email,
                    "password":password,
                    "password_confirmation":confirmPassword,                    
                }

                signUp(data).then(response=>{
                    this.props.onLogingStateResponse(false)
                    if(response.error == true){
                        this.props.onApiErrorResponse(true,response.message)
                    }else{
                        this.props.onApiSuccessResponse(true)
                    }
                   
                })

            } catch (e) {
                console.log(e)
            }
        }

        render() {return (<View/>);}
    }

    class LoginForm extends Component {
        constructor(props){
            super(props);
            this.props.onRef(this)
        }

        _checkLoginValidation = (email,password) => {
           
            if(email != null && password != null ){
                if( email.length > 0 && password.length > 0)
                {
                   this.props.onLoadingResponse(true)
                    this.callTheApi(email,password)
                }else if(email.length == 0){
                    this.props.onErrorResponse(true,false)
                }else if(password.length == 0){
                    this.props.onErrorResponse(false,true)
                }
               
            }else{
                if(email == null && password == null ){
                    this.props.onErrorResponse(true,true)
                
                }else if(email == null ){
                    
                    this.props.onErrorResponse(false,true)
                }else if(password == null ){
                    this.props.onErrorResponse(true,false)
                }
                
            }
        }

       callTheApi(email,password){
        
        try {
            const data = {
                "email": email,
                "password":password,
                "login_by":"email",                    
            }

            
           
            logIn(data).then(response =>{
                this.props.onLoadingResponse(false)
                if(response.error == false){
                    this.props.onApiErrorResponse(true,response.message)
                }else{
                    this.props.onApiSuccessResponse(true,response.data)
                }
               
            }).catch("response data error")

        } catch (e) {
            this.props.onLoadingResponse(false)
            console.log("error")
        }
       }


        render() {return (<View/>);}
    }


    

    class ForgetForm extends Component {
        constructor(props) {
          super(props);
          this.props.onRef(this)
        }
        forgetPasswordValid = (email) => {
            if(email != null  ){
                if( email.length > 0 )
                {
                    this.props.onLoadingResponse(true)
                    this.callTheApi(email)
                }else if(email.length == 0){
                    this.props.onErrorResponse(true)
                }
               
            }else{
                this.props.onErrorResponse(true)
            }
            
        }

        callTheApi = (email) => {
            console.log(email)
            try {
                const data = {
                    "email": email,                    
                }
    
                 
                forgot(data).then(response => {
                    console.debug("forget response data",response)
                    this.props.onLoadingResponse(false)
                    if(response.error == true){
                        this.props.onResponseError(true,response.message)
                    }else{
                        this.props.onResponseSuccess(true)
                    }
                   
                })
    
            } catch (e) {
                console.log(e)
            }
        }
        
        render() {
          return (
            <View>
             
            </View>
          );
        }
      }


    class ResetPasswordForm extends Component {
        constructor(props) {
          super(props);
          this.props.onRef(this);
        }

        _checkForgetValidation = (email,password,confirmPassword) => {
            
            if(  email != null && password != null && confirmPassword != null ){
                if(  email.length > 0 && password.length > 0 && confirmPassword.length > 0){
                   if(confirmPassword == password){
                        this.props.onLogingStateResponse(true)
                        this._resetCall(email,password,confirmPassword)
                   }else{
                       this.props.onConfirmPasswordResponse(true)
                   }
                }else{
                    
                     if(email.length == 0){
                        this.props.onEmailResponse(true)
                    } 
                    if(password.length == 0){
                        this.props.onPasswordResponse(true)
                    }
                    if(confirmPassword.length == 0){
                        this.props.onConfirmResponse(true)
                    }
                }
                
            }else{
              
                if( email == null && password == null && confirmPassword == null){
                    this.props.onErrorResponse(true,true,true,true)
                
                }else{
                   
                     if(email == null){
                        
                        this.props.onEmailResponse(true)
                    } 
                    if(password == null){
                        this.props.onPasswordResponse(true)
                    }
                    if(confirmPassword == null){
                        this.props.onConfirmResponse(true)
                    }
                }
 
            }
        }
        _resetCall = (email,password,confirmPassword) => {
            try {
                const data = {
                    "email": email,
                    "password":password,
                    "password_confirmation":confirmPassword,                    
                }

                resetPsw(data).then(response=>{
                    this.props.onLogingStateResponse(false)
                    if(response.error == true){
                        this.props.onApiErrorResponse(true)
                    }else{
                        this.props.onApiSuccessResponse(true)
                    }
                   
                })

            } catch (e) {
                console.log(e)
            }
        }

      
        render() { return (  <View/>  );
        }
      }
      
 class PostServer extends Component {
  constructor(props) {
    super(props);
    this.props.onRef(this)
  }

  

  render() {
    return (
      <View>
        <Text> PostServer </Text>
      </View>
    );
  }
}






export { SignupForm,ForgetForm,PostServer,LoginForm,ResetPasswordForm}
