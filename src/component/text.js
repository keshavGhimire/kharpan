import React, { Component } from 'react';
import { View, Text,StyleSheet } from 'react-native';

export default class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View >
        <Text {...this.props} style={[this.props.style,styles.text]}>{this.props.children} </Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
    text:{
        fontFamily: 'Poppins-Regular',
    }
  });
  
