import React, { useState, useEffect } from "react";
import {
  View,
  StyleSheet,
  ImageBackground,
  AppRegistry,
  TouchableOpacity,
} from "react-native";
import FastImage from "react-native-fast-image";
import { Row } from "./Widgets";
import AppSettings from "../utils/AppSettings";
import Text from "../component/text";



const OrderCard = (props) => {
  const [item, setitem] = useState([]);
  useEffect(() => {
    setitem(props.item);
  }, []);
  return (
    <View style={styles.item}>
      <View>
       
        <Row innerStyle={{alignItems: 'center'}}>
          <FastImage
            style={{
              width: 80,
              height: 50,
              resizeMode: 'cover',
              marginRight: 5,
            }}
            source={{
              uri: item.image_url,
            }}
          />
          <View style={{paddingTop: 20}}>
            <Text style={styles.heading}>{item.product_name}</Text>

          <Text style={styles.price}>Rs.{item.rate} / {item.unit_code}</Text>
          </View>
        </Row>
        <View style={styles.line} />
        <Row
          innerStyle={{
            justifyContent: 'space-between',
            margin: 10,
            marginHorizontal: 20,
          }}>
          <Row>
            <Text style={[styles.heading, {width: 65}]}>Rs. {item.total}</Text>
            <View style={styles.farmerMarket}>
              <Text style={{color: '#fff', fontWeight: '700'}}>
                {item.product_source}
              </Text>
            </View>
          
          </Row>

          <Text
            style={[
              styles.btnText,
              {fontSize: 16, paddingTop: 2, marginLeft: 3},
            ]}>
            {item.quantity}
          </Text>
        </Row>
      </View>
    </View>
  );
};


export default OrderCard;
const styles = StyleSheet.create({
  item: {
    height: 135,
    width: "98%",
    backgroundColor: AppSettings.itembackGround,
    elevation: 3,
    marginBottom: 18,
    borderRadius: 8,
    alignSelf: "center",
    zIndex: 0,
    // borderWidth: 0.4,
    // borderColor: '#DFD9D9',
  },
  line: {
    height: 0.5,
    width: "90%",
    backgroundColor: "#C1BDBD",
    alignSelf: "center",
    marginTop: 4,
  },
  farmerMarket: {
    height: 25,
    width: 60,
    borderRadius: 3,
    backgroundColor: AppSettings.SecondaryText,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10,
  },
  itembtn: {
    height: 26,
    width: 26,
    borderRadius: 13,
    backgroundColor: "#fafafa",
    justifyContent: "center",
    alignItems: "center",
    elevation: 3,
  },
  innerCircle: {
    height: 22,
    width: 22,
    borderColor: "#B4B4B4",
    borderRadius: 11,
    borderWidth: 1.2,
    justifyContent: "center",
    alignItems: "center",
  },
  heading: {
    height: 40,
    width: 200,
    fontSize: 15,
    fontWeight: "700",
  },
  price: {
    fontSize: AppSettings.secondaryfontsize,
    color: AppSettings.SecondaryText,
    fontWeight: "700",
    paddingTop: 5,
  },
  btnText: {
    fontSize: 18,
    color: "#B8B8B8",
  },
  circle: {
    height: 18,
    width: 18,
    borderRadius: 8,
    backgroundColor: "red",
    alignItems: "flex-end",
    top: 5,
    marginLeft: 12,
  },
});
