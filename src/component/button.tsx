import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {Row, CenterView} from './Widgets';
import AppSettings from '../utils/AppSettings';
import Icon from 'react-native-vector-icons/Feather';
import Text from '../component/text';
class CustomButton extends Component {
  render() {
    return (
      <TouchableOpacity
        style={[styles.container, this.props.style]}
        onPress={this.props.onPress}>
        <CenterView>
          <Row>
            <Text style={styles.textStyle}> {this.props.children} </Text>
            {this.props.loading ? (
              <ActivityIndicator
                size="small"
                color="#ffffff"
                style={{paddingLeft: 5}}
              />
            ) : (
              <Icon
                name="arrow-right"
                size={22}
                color="#ffffff"
                style={{paddingTop: 3}}
              />
            )}
          </Row>
        </CenterView>
      </TouchableOpacity>
    );
  }
}
class DoubleButton extends Component {
  render() {
    return (
      <Row innerStyle={styles.main}>
        <TouchableOpacity
          onPress={this.props.onPressLeft}
          style={[styles.doubleBottom, styles.left]}>
          <View>
            <Text style={[styles.textStyle, {color: '#000'}]}>
              {this.props.left}
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={this.props.onPressRight}
          style={[styles.doubleBottom, styles.right]}>
          <Row>
            <Text style={styles.textStyle}>{this.props.right}</Text>
          </Row>
        </TouchableOpacity>
      </Row>
    );
  }
}

export {CustomButton, DoubleButton};

const styles = StyleSheet.create({
  container: {
    height: 45,
    width: '90%',
    borderRadius: 5,
    elevation: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#44C062',
  },
  main: {
    marginBottom: 15,
    justifyContent: 'space-between',
    borderRadius: 10,
    borderRadius: 20,
  },
  textStyle: {
    color: 'white',
    fontSize: 18,
    fontWeight: '600',
  },
  doubleBottom: {
    flex: 1,
    backgroundColor: '#44C062',
  },
  left: {
    height: 50,
    borderWidth: 3,
    borderColor: '#E6E6E6',
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
  },
  right: {
    height: 50,
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
