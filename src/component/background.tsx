import React from 'react';
import {SafeAreaView, View} from 'react-native';
import AppSettings from '../utils/AppSettings';

const Background = (props) => {
  return (
    <SafeAreaView
      {...props}
      style={{
        flex: 1,
        backgroundColor: AppSettings.backgroundColor,
      }}>
      <View style={{flex: 1, paddingHorizontal: 12}}>{props.children}</View>
    </SafeAreaView>
  );
};
export default Background;
