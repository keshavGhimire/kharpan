import React from 'react';
import {StyleSheet} from 'react-native';
import {CenterView} from './Widgets';
import Text from './text';
import AppSettings from '../utils/AppSettings';
import Icon from 'react-native-vector-icons/FontAwesome5';

const NoData = (props) => (
  <CenterView innerStyle={{background: AppSettings.itembackGround,flex:0}}>
    <CenterView innerStyle={styles.circle}>
      <Icon name="file-alt" size={35} />
    </CenterView>
<Text style={styles.text}>{props.heading}</Text>
<Text style={styles.secText}>{props.text}</Text>
  </CenterView>
);

export default NoData;
const styles = StyleSheet.create({
  circle: {
    flex:0,
    height: 140,
    width: 140,
    borderRadius: 70,
    backgroundColor: '#ffffff',
    elevation:1
  },
  text: {
    fontSize: 20,
    fontWeight: '600',
    marginTop: 20,
  },
  secText: {
    fontSize: 14,
   
    marginTop: 8,
  },
});
