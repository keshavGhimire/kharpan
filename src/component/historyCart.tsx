import React, {useState, useEffect} from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import {Row} from './Widgets';
import AppSettings from '../utils/AppSettings';
import Icon from 'react-native-vector-icons/Entypo';
import Icons from 'react-native-vector-icons/FontAwesome5';

const HistoryCart = props => {
  const [item, setitem] = useState([]);
  useEffect(() => {
    setitem(props.item);
  }, []);
  const Navigate = () => {
    // console.log('hello');
    // Navigation.push(props.componentId, {
    //   component: {
    //     name: 'orderDetails',
    //     passProps: {
    //       name: item.masterid,
    //     },
    //   },
    // });
    props.navigation.navigate('orderDetails', {name: item.masterid});
  };
  return (
    <TouchableOpacity onPress={() => Navigate(item)}>
      <Row innerStyle={styles.item}>
        <Icons name="shopping-bag" size={40} color="#BEBBBB" />
        <View style={{width: '60%'}}>
          <Text style={[styles.text, {fontWeight: 'bold'}]}>
            {' '}
            {item.order_date}
          </Text>

          <Text style={styles.text}>Order Id : {item.order_no}</Text>
          <Text style={styles.text}>
            Total Amount :RS. {item.total_amt === null ? 'NAN' : item.total_amt}
          </Text>
          <Text numberOfLines={1} style={styles.text}>
            Remarks : {item.order_remarks}
          </Text>
        </View>
        <Icon name="chevron-small-right" size={40} color="#BEBBBB" />
      </Row>
    </TouchableOpacity>
  );
};

export default HistoryCart;
const styles = StyleSheet.create({
  item: {
    height: 100,
    width: '99%',
    backgroundColor: AppSettings.itembackGround,
    elevation: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    alignSelf: 'center',
    marginVertical: 8,
  },
  text: {
    color: '#707070',
    fontSize: 14,
    fontWeight: '300',
    fontFamily: 'Poppins-Regular',
  },
});
