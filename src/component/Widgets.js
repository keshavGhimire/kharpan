import React, {Component} from 'react';
import {View, StyleSheet, SafeAreaView} from 'react-native';


class CenterView extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.innerStyle]}>
        {this.props.children}
      </View>
    );
  }
}

class Row extends Component {
  render() {
    return (
      <View
        style={[
          styles.row,
          this.props.innerStyle,
          this.props.type == 'button' ? styles.buttonStyle : null,
        ]}>
        {this.props.children}
      </View>
    );
  }
}

class Column extends Component {
  render() {
    return (
      <View style={[styles.column, this.props.innerStyle]}>
        {this.props.children}
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
    borderRadius: 10,
  },
  column: {
    flexDirection: 'column',
  },

 
});

export {CenterView, Row, Column};
