import axios from 'axios';

const headers = {
  headers: {
    'Access-Control-Allow-Origin': '*',
  },
};

export async function getRequests(url) {
  try {
    // const headers = await getHeaders();
    const response = await axios.get(url);
    const responseData = response.data;
    return responseData;
  } catch (err) {
    console.log(err);
    return null;
  }
}

export async function postRequests(url, body) {
  try {
    // const headers = await getHeaders();
    // console.log(headers)
    const response = await axios.post(url, body, {headers});
    const responseData = response.data;
    console.log(responseData);
    return responseData;
  } catch (err) {
    return null;
  }
}

export async function postTokenlessRequest(url, body) {
  try {
    const response = await axios.post(url, body);
    const responseData = response.data;
    console.log(responseData);
    return responseData;
  } catch (err) {
    return null;
  }
}

export async function putRequests(url, body) {
  try {
    // const headers = await getHeaders();
    const response = await axios.put(url, body, {headers});
    const responseData = response.data;
    console.log(responseData);
    return responseData;
  } catch (err) {
    return null;
  }
}
