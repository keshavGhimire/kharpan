import React, {useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  Modal,
  Alert,
  FlatList,
} from 'react-native';
import Header from '../component/Header';
import {Row} from '../component/Widgets';
import {DoubleButton} from '../component/button';
import Text from '../component/text';
import AppSettings from '../utils/AppSettings';
import DialogInput from 'react-native-dialog-input';
import {useSelector, useDispatch} from 'react-redux';
import NoData from '../component/noData';
import {KharpannItem} from '../component/kharpann/KharpannItem';
import {CHECK_OUT, HOME_URL} from '../utils/api';
import {getRequests, postTokenlessRequest} from '../network';
import {clearCart, getList, loadDefault} from '../redux/action/listAction';
import {homeRoot} from '../utils/setRoot';
import {Congrats} from '../component/congratsmodal';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
const Cart = props => {
  const [isDialogVisible, setisDialogVisible] = useState(false);
  const [orderID, setOrderID] = useState('');
  const [congratsModal, setcongratsModal] = useState(false);
  const [ScheduleData, setScheduleData] = useState([]);
  const list = useSelector(state => state.kharpan.cartList);
  const total = list.reduce(
    (acc, item) => acc + item.original_rate * item.quantity,
    0,
  );
  const dispatch = useDispatch();
  useEffect(() => {
   
    bannerData();
  }, []);
  const _renderItem = ({item}) => {
    return <KharpannItem item={item} navigation={props.navigation}/>;
  };
  const showModel = ({item}) => {
    setcongratsModal(true);
    
   getNewItem();
   dispatch(clearCart());
    setTimeout(() => {
      props.navigation.navigate('home');
      setcongratsModal(false);
     
    }, 1000);
  };

  const getNewItem = async () => {
    try {
      const url = HOME_URL+'getproductlistall';
      const response = await axios.get(url);
      const list = response.data.map(item => {
        item.quantity = 0;
        return item;
      });
      dispatch(getList(list));
    } catch (err) {}
  }

  const bannerData = async () => {
    const id = await AsyncStorage.getItem('userID');
    const url = HOME_URL + 'gethomedata/' + id + "/1/0/'";
    const res = await getRequests(url);
    if (res) {
      const datas = res.ScheduleData;
      setScheduleData(datas);
    } else {
      console.log('error');
    }
  };

  const checkOut = async remark => {
    const id = await AsyncStorage.getItem('userID');
    let submitList = [];
    list.map(item => {
      submitList.push({
        productid: item.id,
        cartqty: item.quantity,
        rate: item.original_rate,
        itemsum: item.original_rate * item.quantity,
      });
    });

    const url = CHECK_OUT;
    const data = {
      customerid: id,
      grandtotal: total,
      orderremarks: remark,
      detailList: submitList,
    };
    // console.log('hit hunu aahghko data', data);
    const res = await postTokenlessRequest(url, data);
    if (res !== null) {
      if (res.StatusCode === 200) {
        // console.log("res")
        setOrderID(res);
        showModel(res);
      } else {
      }
    }
  };
  const cancelCart = () => {
    Alert.alert(
      'Hold on!',
      'You will loose all items on your cart. Do you want to continue?',
      [
        {
          text: 'Cancel',
          onPress: () => null,
          style: 'cancel',
        },
        {
          text: 'YES',
          onPress: () => {
           props.navigation.navigate('home');
            dispatch(clearCart());
          },
        },
      ],
    );
  };
  return (
    <SafeAreaView style={{flex: 1, marginHorizontal: 12}}>
      <Modal visible={congratsModal} transparent>
        <Congrats
          orderid={orderID?.Id}
          title={'Thank you for Placing Your Order'}
        />
      </Modal>
      <Header
        componentId={props.componentId}
        icon={require('../../src/icons/left-back.png')}
        heading={'My cart'}
        profile={true}
        navigation={props.navigation}
      />
      <View style={{flex: 1}}>
        {list.length==0 ? (
          <View style={{marginTop: '30%'}}>
            <NoData
              heading="No Item "
              text="You don't have any item in your cart."
            />
          </View>
        ) : (
          <View style={{flex: 1}}>
            <FlatList
              data={list}
              renderItem={_renderItem}
              keyExtractor={index => index.toString()}
              showsVerticalScrollIndicator={false}
            />
            <Row innerStyle={styles.priceDetails}>
              <Text
                style={[
                  styles.text,
                  {fontSize: 16, color: '#000', fontWeight: 'bold'},
                ]}>
                Total Payable
              </Text>
              <Text
                style={[
                  styles.text,
                  {fontSize: 16, color: '#000', fontWeight: 'bold'},
                ]}>
                Rs. {total.toFixed(2)}
              </Text>
            </Row>
            <DoubleButton
              left="Cancel"
              right="Order Now"
              onPressRight={() => setisDialogVisible(true)}
              onPressLeft={() => cancelCart()}
            />
            <DialogInput
              isDialogVisible={isDialogVisible}
              title={'Check Out'}
              message={
                'On your area delivery will be on' +
                ScheduleData.map(item => {
                  return (
                    '\n' +
                    item.WeekdayName +
                    ' (' +
                    item.starttime +
                    ' -' +
                    item.endtime +
                    ')'
                  );
                })
              }
              hintInput={'Write some special instruction .....'}
              submitInput={inputText => {
                checkOut(inputText);
                // setisDialogVisible(false);
              }}
              closeDialog={() => {
                setisDialogVisible(false);
              }}
            />
          </View>
        )}
      </View>
    </SafeAreaView>
  );
};

export default Cart;
const styles = StyleSheet.create({
  item: {
    flex: 2,
  },
  priceDetails: {
    paddingVertical: 5,
    paddingHorizontal: 10,
    justifyContent: 'space-between',
  },
  priceBox: {
    flex: 1,
    backgroundColor: '#fff',
    borderWidth: 0.1,
    borderColor: 'grey',
    borderRadius: 3,
    paddingHorizontal: 10,
    justifyContent: 'center',
  },
  priceSegment: {
    flex: 1.5,
    paddingVertical: 10,
  },
  text: {
    color: AppSettings.SecondaryText,
    fontSize: AppSettings.secondaryfontsize,
    fontWeight: '600',
  },
  line: {
    height: 0.5,
    width: '90%',
    backgroundColor: '#C1BDBD',
    alignSelf: 'center',
  },
});
{
  /* <View style={styles.priceSegment}>
        <Text
          style={[
            styles.text,
            {
              fontSize: 16,
              marginLeft: -5,
              paddingVertical: 10,
              fontWeight: 'bold',
            },
          ]}>
          Price details
        </Text>
        <View style={styles.priceBox}>
          <Row innerStyle={styles.priceDetails}>
            <Text style={styles.text}>Sub Total</Text>
            <Text style={styles.text}>Rs. 75.61</Text>
          </Row>
          <Row innerStyle={styles.priceDetails}>
            <Text style={styles.text}>Discount</Text>
            <Text style={[styles.text, {color: 'red'}]}>- Rs. 10.61</Text>
          </Row>
          <Row innerStyle={styles.priceDetails}>
            <Text style={styles.text}>Estimated Tax</Text>
            <Text style={styles.text}>Rs. 75.61</Text>
          </Row>
          <Row innerStyle={styles.priceDetails}>
            <Text style={styles.text}>Delivery</Text>
            <Text style={[styles.text, {color: '#44C062'}]}>Free</Text>
          </Row>
          <View style={styles.line} />
        </View>
      </View> */
}
