import React from 'react';
import {
  Text,
  ImageBackground,
  StyleSheet,
  Dimensions,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import {useDispatch} from 'react-redux';
import {HEIGHT, WIDTH} from '../component/metrics';
import Swiper from 'react-native-swiper';

const slides = [
  {
    key: 'k1',

    image: require('../icons/wt-1.jpg'),
  },
  {
    key: 'k2',

    image: require('../icons/wt-2.jpg'),
  },
  {
    key: 'k3',

    image: require('../icons/wt-3.jpg'),
  },
];

const styles = StyleSheet.create({
  skip: {
    color: '#007aff',
    textAlign: 'right',
    fontSize: 14,
    fontFamily: 'Poppins-Regular',
    padding: 10,
  },
});

const WalkThrough = props => {
  const change = () => {
    props.navigation.replace('HomeStack');
  };
  const Index = index => {
    index == 2
      ? setTimeout(() => {
          change();
        }, 1500)
      : null;
  };
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#cac8c9'}}>
      <Swiper showsPagination loop={false} onIndexChanged={Index}>
        {slides.map(data => {
          return (
            <ImageBackground
              key={data.key}
              style={{height: HEIGHT, width: WIDTH}}
              source={data.image}>
              <TouchableOpacity onPress={() => change()}>
                <Text style={styles.skip}>Skip</Text>
              </TouchableOpacity>
            </ImageBackground>
          );
        })}
      </Swiper>
    </SafeAreaView>
  );
};
export default WalkThrough;
