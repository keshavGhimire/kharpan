import React, {useEffect, useState} from 'react';
import {Text, ImageBackground, Modal, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {useDispatch, useSelector} from 'react-redux';
import {CustomButton} from '../component/button';
import {AfterRegister} from '../component/congratsmodal';
import {SignInInput} from '../component/inputText';
import {HEIGHT, WIDTH} from '../component/metrics';
import {CenterView, Row} from '../component/Widgets';
import {postTokenlessRequest} from '../network';
import {openModel} from '../redux/action/colorAction';
import {REGISTER} from '../utils/api';
import {SnackMsg} from '../utils/SnackMsg';
import {validatePhone} from '../utils/validate';
import {rootPush} from '../utils/setRoot';
const SignUp = props => {
  const dispatch = useDispatch();
  const colorModel = useSelector(state => state.color.model);
  const [Phone, setPhone] = React.useState('');
  const [Name, setName] = React.useState('');
  const [Address, setAddress] = React.useState('');
  const [loading, setloading] = useState(false);
  // const [modelView, setModel] = useState(true);
  const onPressRegister = async () => {
    setloading(true);
    if (validatePhone(Phone)) {
      const url = REGISTER;
      const data = {
        name: Name,
        phone: Phone,
        address: Address,
      };
      const res = await postTokenlessRequest(url, data);
      if (res !== null) {
        if (res.StatusCode === 200) {
          setloading(false);
          dispatch(openModel());
        }
      }
    } else {
      SnackMsg('enter valid phone number');
      setloading(false);
    }
  };
  goBack = () => {
    rootPush('signIn');
  };
  return (
    <ImageBackground
      style={{height: HEIGHT, width: WIDTH, resizeMode: 'center'}}
      source={require('../icons/loginBg.png')}>
      <Modal visible={colorModel} transparent>
        <AfterRegister
          title={
            'Thank you for submitting your registration.\n We will contact you soon!'
          }
        />
      </Modal>
      <CenterView innerStyle={{flex: 0.5}}>
        <FastImage
          resizeMode={FastImage.resizeMode.cover}
          style={{width: 211, height: 256}}
          source={require('../icons/logo.png')}
        />
      </CenterView>
      <View style={{marginHorizontal: 25, flex: 1, top: -20}}>
        <SignInInput
          placeholder={'NAME'}
          value={Name}
          onChangeText={text => setName(text)}
        />

        <SignInInput
          placeholder={'PHONE'}
          keyboardType={'numeric'}
          value={Phone}
          onChangeText={text => setPhone(text)}
        />
        <SignInInput
          placeholder={'ADDRESS'}
          value={Address}
          onChangeText={text => setAddress(text)}
        />
        <CustomButton
          onPress={() => onPressRegister()}
          loading={loading}
          style={{alignSelf: 'center', width: '85%', marginTop: 10}}>
          Submit
        </CustomButton>
        <CustomButton
          onPress={() => goBack()}
          loading={loading}
          style={{
            alignSelf: 'center',
            width: '85%',
            marginTop: 10,
            backgroundColor: 'red',
          }}>
          Cancel
        </CustomButton>
        <Text style={{textAlign: 'center', fontSize: 30, fontFamily: 'Debby'}}>
          Or {'\n'}Please Contact:{' '}
        </Text>
        <Text style={{textAlign: 'center', fontSize: 22}}>
          9801166566, 9801462945{' '}
        </Text>
      </View>
    </ImageBackground>
  );
};

export default SignUp;
