import * as React from 'react';
import {StyleSheet, Dimensions, Text, SafeAreaView} from 'react-native';
import {TabView, TabBar} from 'react-native-tab-view';
import Header from '../component/Header';
import Pending from './Order/Pending';
import Process from './Order/Process';
import Delivered from './Order/Delivered';
import Packed from './Order/Packed';

const initialLayout = {width: Dimensions.get('window').width};
export default function Order(props) {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'first', title: 'Pending'},
    {key: 'second', title: 'Processed'},
    {key: 'third', title: 'Packed'},
    {key: 'fourth', title: 'Delivered'},
  ]);

  const renderScene = ({route}) => {
    switch (route.key) {
      case 'first':
        return <Pending componentId={props.componentId} navigation={props.navigation}/>;
      case 'second':
        return <Process componentId={props.componentId} navigation={props.navigation}/>;
      case 'third':
        return <Packed componentId={props.componentId} navigation={props.navigation}/>;
      default:
        return <Delivered componentId={props.componentId} navigation={props.navigation}/>;
    }
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fafafa'}}>
      <Header
        style={{marginHorizontal: 12}}
        componentId={props.componentId}
        icon={require('../../src/icons/left-back.png')}
        heading={'Order History'}
        rejected={true}
        profile={true}
        navigation={props.navigation}
      />
      <TabView
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={initialLayout}
        style={{top: -15}}
        renderTabBar={(props) => (
          <TabBar
            {...props}
            style={{backgroundColor: '#fafafa', marginLeft: -5}}
            indicatorStyle={{backgroundColor: '#A4A5A6'}}
            activeColor="#707070"
            inactiveColor="#BEB6B6"
            tabStyle={{width: 'auto'}}
            pressColor="rgba(179,229,255, 0.5)"
            renderLabel={({route, focused, color}) => (
              <Text style={{color, margin: 8, fontWeight: '700'}}>
                {route.title}
              </Text>
            )}
          />
        )}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
});
