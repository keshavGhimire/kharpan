import React, {useState, useEffect} from 'react';
import {Text, View, StyleSheet, SafeAreaView} from 'react-native';

import FastImage from 'react-native-fast-image';
import {Row, CenterView} from '../component/Widgets';
import Header from '../component/Header';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {GET_CONTACT} from '../utils/api';
import {getRequests} from '../network';

const Contact = props => {
  const [Contact, setContact] = useState([]);
  useEffect(() => {
    getContact();
  }, []);
  getContact = async () => {
    const url = GET_CONTACT;
    const res = await getRequests(url);
    if (res) {
      mapData(res);
    } else {
      console.log('error');
    }
  };
  const [region, setRegion] = useState({
    latitude: 27.7233,
    longitude: 85.324,
    latitudeDelta: 0.011,
    longitudeDelta: 0.013,
  });
  const mapData = res =>
    setContact([
      {
        key: 'k1',
        title: res.address,
        image: require('../icons/info.png'),
        // onpress:
      },

      {
        key: 'k3',
        title: '9801166566, 9801462945',
        image: require('../icons/phone.png'),
        // onpress:
      },
      {
        key: 'k4',
        title: res.email,
        image: require('../icons/mail.png'),
      },
    ]);
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fafafa'}}>
      <Header
        style={{paddingHorizontal: 12}}
        componentId={props.componentId}
        icon={require('../../src/icons/left-back.png')}
        heading={'Contact'}
        navigation={props.navigation}
      />
      <CenterView>
        {/* <MapView
        style={{flex: 1, top: -10}}
        region={region}
        onRegionChangeComplete={(region) => setRegion(region)}>
        <Marker coordinate={{latitude: 27.7172, longitude: 85.324}}> */}

        <View
          style={{
            height: 280,
            paddingTop: 50,
            paddingVertical: 10,
            paddingHorizontal: 30,
            backgroundColor: '#ffffff',
            borderColor: '#eee',
            borderRadius: 5,
            elevation: 10,
            position: 'relative',
            zIndex: 0,
          }}>
          <FastImage
            resizeMode={FastImage.resizeMode.contain}
            source={require('../icons/kharpanRound.png')}
            style={{
              height: 100,
              width: 100,
              borderRadius: 50,

              alignSelf: 'center',
              marginTop: -50,
              position: 'absolute',
              zIndex: 9999,
            }}
          />

          <Text style={styles.heading}>Kharpan Trade Pvt. Ltd.</Text>
          {Contact.map(data => {
            return (
              <Row
                key={data.key}
                innerStyle={{
                  justifyContent: 'space-between',
                  paddingVertical: 6,
                }}>
                <Row>
                  <FastImage
                    style={styles.iconImage}
                    source={data.image}
                    resizeMode={FastImage.resizeMode.contain}
                  />
                  <Text style={styles.userText}>{data.title}</Text>
                </Row>
              </Row>
            );
          })}
          <Row
            innerStyle={{
              width: '60%',
              justifyContent: 'space-around',
              alignItems: 'center',
              alignSelf: 'center',
              marginTop: 20,
            }}>
            <Icon name="linkedin" size={22} />
            <Icon name="facebook-square" size={22} />
            <Icon name="envelope" size={22} />
            <Icon name="twitter-square" size={22} />
          </Row>
        </View>

        <Icon
          name="sort-down"
          size={50}
          color={'#ffffff'}
          style={{alignSelf: 'center', top: -31}}
        />

        {/* </Marker>
      </MapView> */}
      </CenterView>
    </SafeAreaView>
  );
};
export default Contact;

const styles = StyleSheet.create({
  userText: {
    color: '#707070',
    fontSize: 15,
    fontWeight: '500',
    fontFamily: 'Poppins-Regular',
  },
  heading: {
    color: '#707070',
    fontSize: 19,
    fontWeight: '300',
    fontFamily: 'Poppins-Regular',
    marginBottom: 2,
  },
  iconImage: {width: 18, height: 18, marginRight: 10},
});
