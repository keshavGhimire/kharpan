import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import Background from '../component/background';
import Text from '../component/text';
import AppSettings from '../utils/AppSettings';
import {CustomButton} from '../component/button';
import {RadioButton} from 'react-native-paper';
import Header from '../component/Header';
import {useSelector} from 'react-redux';
import {postTokenlessRequest} from '../network';
import {HOME_URL} from '../utils/api';
const Delivery = (props) => {
  const Id = useSelector((state) => state.walkthrough.userID);
  const [checked, setChecked] = useState('door');
  const [loading, setloading] = useState(false);
  const submitData = async () => {
    setloading(true);
    const url = HOME_URL + 'profile/updatenotify';
    const data = {
      customerid: Id,
      notifystatus: checked,
    };
    const res = await postTokenlessRequest(url, data);
    console.log(res);
  };

  return (
    <Background style={{backgroundColor: '#ffffff'}}>
      <Header
        componentId={props.componentId}
        icon={require('../../src/icons/left-back.png')}
        heading={'On Delivery'}
      />
      <View style={{flex: 5}}>
        <Text style={styles.heading}>Wait ! Just a second.</Text>
        <Text
          style={[
            styles.label,
            {fontWeight: '100', margin: 0, marginVertical: 20},
          ]}>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard.
        </Text>
        <View style={{marginVertical: 20}} />
        <View style={styles.checkboxContainer}>
          <Text style={styles.label}>Leave on door.</Text>
          <RadioButton
            value="door"
            status={checked === 'door' ? 'checked' : 'unchecked'}
            color="green"
            onPress={() => setChecked('door')}
          />
        </View>
        <View style={styles.checkboxContainer}>
          <Text style={styles.label}>Give a call.</Text>
          <RadioButton
            value="call"
            color={AppSettings.signinbutton}
            status={checked === 'call' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('call')}
          />
        </View>
        <View style={styles.checkboxContainer}>
          <Text style={styles.label}>Leave on gate.</Text>
          <RadioButton
            value="gate"
            color={AppSettings.signinbutton}
            status={checked === 'gate' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('gate')}
          />
        </View>
      </View>
      <View style={{flex: 1, alignItems: 'center'}}>
        <CustomButton loading={loading} onPressonPress={() => submitData()}>
          Save and proceed.
        </CustomButton>
      </View>
    </Background>
  );
};

const styles = StyleSheet.create({
  checkboxContainer: {
    height: 45,
    width: '98%',
    alignSelf: 'center',
    backgroundColor: '#E9E8E8',

    flexDirection: 'row',
    marginBottom: 20,
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  checkbox: {
    alignSelf: 'center',
  },
  label: {
    margin: 12,
    textAlign: 'left',
    color: AppSettings.SecondaryText,
    fontWeight: '700',
  },
  heading: {
    fontSize: 24,
    marginTop: '10%',
    fontWeight: '700',
  },
});

export default Delivery;
