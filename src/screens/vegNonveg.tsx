import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import Background from '../component/background';
import Text from '../component/text';
import AppSettings from '../utils/AppSettings';
import {CustomButton} from '../component/button';
import {RadioButton} from 'react-native-paper';
import Header from '../component/Header';
import {useSelector} from 'react-redux';
import {BASE_URL, CHNAGE_PRODUTCT_PREF, GET_PROFILE} from '../utils/api';
import {getRequests, postTokenlessRequest} from '../network';
import {SnackMsg} from '../utils/SnackMsg';

const VegNonVeg = (props) => {
  const [checked, setChecked] = useState('');
  const [loading, setloading] = useState(false);
  const Id = useSelector((state) => state.walkthrough.userID);
  useEffect(() => {
    getProfile();
  }, []);
  getProfile = async () => {
    const url = GET_PROFILE + Id;

    const res = await getRequests(url);
    if (res) {
      const data = res.product_type;
      setChecked(data.toString());
      console.log('checked', checked);
    } else {
      console.log('error');
    }
  };
  onSubmit = async () => {
    setloading(true);
    const url = BASE_URL + CHNAGE_PRODUTCT_PREF;
    const data = {
      customer_id: Id,
      product_type: parseInt(checked),
    };

    const res = await postTokenlessRequest(url, data);
    if (res !== null) {
      if (res.StatusCode === 200) {
        setloading(false);
        SnackMsg('successful');
      }
    }
  };
  return (
    <Background>
      <Header
        componentId={props.componentId}
        icon={require('../../src/icons/left-back.png')}
        heading={'Veg Or non Veg ?'}
        navigation={props.navigation}
      />
      <View style={{flex: 5}}>
        <Text style={styles.heading}>Change your preference type</Text>
        {/* <Text
          style={[
            styles.label,
            { fontWeight: '100', margin: 0, marginVertical: 20 },
          ]}>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard.
        </Text> */}
        <View style={{marginVertical: 20}} />
        <View style={styles.checkboxContainer}>
          <Text style={styles.label}>I am vegetarian but I eat egg</Text>
          <RadioButton
            value="3"
            status={checked === '3' ? 'checked' : 'unchecked'}
            color="green"
            onPress={() => setChecked('3')}
          />
        </View>
        <View style={styles.checkboxContainer}>
          <Text style={styles.label}>I am non vegetarian</Text>
          <RadioButton
            value="2"
            color={AppSettings.signinbutton}
            status={checked === '2' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('2')}
          />
        </View>
        <View style={styles.checkboxContainer}>
          <Text style={styles.label}>I am vegetarian</Text>
          <RadioButton
            value="1"
            color={AppSettings.signinbutton}
            status={checked === '1' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('1')}
          />
        </View>
      </View>
      <View style={{flex: 1, alignItems: 'center'}}>
        <CustomButton onPress={onSubmit} loading={loading}>
          Save and proceed.
        </CustomButton>
      </View>
    </Background>
  );
};

const styles = StyleSheet.create({
  checkboxContainer: {
    height: 45,
    width: '98%',
    alignSelf: 'center',
    backgroundColor: '#E9E8E8',

    flexDirection: 'row',
    marginBottom: 20,
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  checkbox: {
    alignSelf: 'center',
  },
  label: {
    margin: 12,
    textAlign: 'left',
    color: AppSettings.SecondaryText,
    fontWeight: '700',
  },
  heading: {
    fontSize: 24,
    marginTop: '10%',
    fontWeight: '700',
  },
});

export default VegNonVeg;
