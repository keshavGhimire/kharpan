import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useEffect, useRef, useState} from 'react';
import {
  ActivityIndicator,
  FlatList,
  Modal,
  Platform,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Header from '../component/Header';
import {CenterView, Row} from '../component/Widgets';
import ColorCode from '../component/colorCode';
import VegNonVeg from '../component/vegNon';
import {getRequests} from '../network';
import AppSettings from '../utils/AppSettings';
import {CATEGORY, HOME_URL} from '../utils/api';
import Icon from 'react-native-vector-icons/Ionicons';
import Icons from 'react-native-vector-icons/FontAwesome5';
import Swiper from 'react-native-swiper';
import FastImage from 'react-native-fast-image';
import {KharpannItem} from '../component/kharpann/KharpannItem';
import {BorderlessButton} from 'react-native-gesture-handler';
import {Menu, MenuItem, MenuDivider} from 'react-native-material-menu';
import {FAB, RadioButton} from 'react-native-paper';
import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';
import {getList, loadDefault, searchItem} from '../redux/action/listAction';

const Landing = ({navigation}) => {
  const dispatch = useDispatch();
  const [nextDelivery, setnextDelivery] = useState([]);
  const [menuExpanded, setmenuExpanded] = React.useState(false);
  const [input, setInput] = useState('');
  const [Category, setCategory] = useState([]);
  const [Choose, setChoose] = useState(1);
  const [bannerImage, setbannerImage] = useState([]);
  const [ScheduleData, setScheduleData] = useState([]);
  const [SelectedCategory, setSelectedCategory] = useState('ALL');
  const [refreshing, setRefreshing] = useState(false);
  const [activity, setActivity] = useState(true);
  const lists = useSelector(state => state.kharpan.lists);
  var list = useSelector(state => state.kharpan.cartList);
  const sucess = useSelector(state => state.log.success);

  useEffect(() => {
    categoryApi();
    bannerData();
  }, [list == 0]);

  const categoryApi = async () => {
    const url = HOME_URL + CATEGORY;
    const res = await getRequests(url);
console.log(url)
    if (res) {
      setCategory(res);
    } else {
      console.log('error category');
    }
  };
  const bannerData = async () => {
    var id = 0;
    const data = await AsyncStorage.getItem('userID');
    if (data) {
      id = data;
    }
    const url = HOME_URL + 'gethomedata/' + id + "/1/0/'";
    console.log(url);
    const res = await getRequests(url);

    if (res) {
      const data = res.BannerData;
      const datas = res.ScheduleData;
      const dataNext = res.NextDeliveryData;
      dataNext.map(item => {
        setnextDelivery(item);
      });
      setbannerImage(data);
      setScheduleData(datas);
      var mapData = [];
      weekArray = datas;
      weekArray.map(item => {
        mapData.push(item.weekday);
      });
      // dispatch(addDayArray(mapData));
      setActivity(false);
    } else {
      console.log('error home');
    }
  };

  const _renderItem = ({item}) => {
    return item.product_source_id == Choose ? (
      <KharpannItem item={item} navigation={navigation} sucess={sucess} />
    ) : null;
  };

  const hideMenu = id => {
    setSelectedCategory(id);
    if (id == 'ALL') {
      // dispatch(loadDefault());
    } else {
      // dispatch(filter(id));
    }
    setmenuExpanded(false);
  };

  const renderHeader = () => {
    return (
      <Row
        innerStyle={{
          alignItems: 'center',
          paddingHorizontal: 8,
          justifyContent: 'space-between',
          marginVertical: 4,
        }}>
        <Menu
          visible={menuExpanded}
          anchor={
            <TouchableOpacity
              style={styles.button}
              onPress={() => setmenuExpanded(true)}>
              <Icons name="ellipsis-v" size={25} />
            </TouchableOpacity>
          }
          onRequestClose={() => {}}>
          {Category.length > 0
            ? Category.map(item => {
                return (
                  <View key={item.id}>
                    <MenuItem onPress={() => hideMenu(item.category_name)}>
                      {item.category_name}
                    </MenuItem>
                    <MenuDivider />
                  </View>
                );
              })
            : null}
        </Menu>

        <RadioButton.Group
          onValueChange={newValue => setChoose(newValue)}
          value={Choose}>
          <Row>
            <Row innerStyle={styles.farmerMarket}>
              <RadioButton.Android value={1} />
              <Text
                style={Choose == 1 ? {fontSize: 18, fontWeight: '700'} : null}>
                Farmer
              </Text>
            </Row>
            <Row innerStyle={styles.farmerMarket}>
              <RadioButton.Android value={2} />
              <Text
                style={Choose == 2 ? {fontSize: 16, fontWeight: '700'} : null}>
                Market
              </Text>
            </Row>
            <Row innerStyle={styles.farmerMarket}>
              <RadioButton.Android value={3} />
              <Text
                style={Choose == 3 ? {fontSize: 16, fontWeight: '700'} : null}>
                Bakery
              </Text>
            </Row>
          </Row>
        </RadioButton.Group>

        <View />
      </Row>
    );
  };

  const onRefresh = React.useCallback(() => {}, []);
  const search = (text) => {
    setInput(text);
    if (text.length > 1) {
      dispatch(searchItem(text));
    }
    if (text === '') {
      dispatch(loadDefault());
    }
  };
  return (
    <SafeAreaView
      style={{flex: 1, backgroundColor: '#fafafa', marginHorizontal: 10}}>
      {activity ? (
        <CenterView>
          <ActivityIndicator size="large" color="#00ff00" />
        </CenterView>
      ) : (
        <View>
          <Modal animationType="slide" transparent={true} visible={false}>
            <View style={styles.modalView}>
              <VegNonVeg />
            </View>
          </Modal>
          <Modal animationType="slide" transparent={true} visible={false}>
            <ColorCode />
          </Modal>

          <Header
            icon={require('../../src/icons/Menu.png')}
            heading={'Kharpan'}
            profile={true}
            sidemenu={true}
            delivery={true}
            style={{paddingHorizontal: 4}}
            navigation={navigation}
          />

          {nextDelivery?.Text2 ? (
            <Text
              style={{
                fontSize: 13,
                top: -8,
                fontWeight: '700',

                textAlign: 'center',
                paddingBottom: 5,
              }}>
              {nextDelivery.Text2}
            </Text>
          ) : null}
          <View style={styles.inputView}>
            <TextInput
              placeholder="Search for ......"
              value={input}
              onChangeText={text => search(text)}
              style={styles.input}
            />
            <Icon name="search" size={20} />
          </View>

          <View style={{height: 200}}>
            <Swiper
              autoplay={true}
              showsPagination={false}
              loop={true}
              removeClippedSubviews={false}
              showsButtons={false}
              scrollEnabled={false}
              autoplayDirection={false}
              autoplayTimeout={2}>
              {bannerImage.map(item => {
                return (
                  <FastImage
                    key={item.id}
                    resizeMode={FastImage.resizeMode.cover}
                    source={require('../../src/icons/banner_1.jpg')}
                    style={styles.image}
                  />
                );
              })}
            </Swiper>
          </View>

          <FlatList
            data={lists}
            renderItem={_renderItem}
            ListHeaderComponent={renderHeader}
            keyExtractor={item => item?.id?.toString()}
            showsVerticalScrollIndicator={false}
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
          />
        </View>
      )}
      {sucess ? (
        <View style={styles.cart}>
          {/* {list.length > 0 && ( */}
          <View
            style={{
              height: 22,
              width: 22,
              borderRadius: 11,
              right: 6,
              top: 8,
              backgroundColor: 'red',
              justifyContent: 'center',
              zIndex: 999,
              alignItems: 'center',
            }}>
            <Text
              style={{
                left: 2,
                color: AppSettings.white,
                fontWeight: 'bold',
                fontSize: 15,
              }}>
              {list.length}
            </Text>
          </View>
          {/* )} */}

          <FAB
            style={styles.roundCart}
            small
            icon="cart"
            onPress={() => navigation.navigate('cart')}
          />
        </View>
      ) : null}
    </SafeAreaView>
  );
};

export default Landing;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  roundCart: {
    height: 50,
    width: 50,
    borderRadius: 25,
    backgroundColor: AppSettings.white,
    elevation: 3,
    paddingVertical: 5,
    shadowColor: '#000000',
    shadowOpacity: 0.3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cart: {
    position: 'absolute',
    height: 60,
    width: 60,
    zIndex: 999,
    elevation: 1,
    right: 23,
    bottom: 70,
  },
  inputView: {
    height: 40,
    width: '99%',
    marginTop: -8,
    marginBottom: 10,
    borderRadius: 20,
    borderWidth: 0.1,
    borderColor: '#495057',
    // backgroundColor: 'red',
    backgroundColor: '#f1f3f5',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 30,
    alignItems: 'center',
    alignSelf: 'center',
  },

  input: {
    width: '100%',
    fontSize: 18,
    color: '#000000',
    fontFamily: Platform.OS === 'android' ? 'Roboto' : 'Arial',
  },
  button: {
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
  },
  farmerMarket: {
    alignItems: 'center',
  },
  image: {
    height: 200,
    width: '100%',
  },
  modalView: {
    flex: 1,
    margin: 5,
    backgroundColor: AppSettings.backgroundColor,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  chooseLeftt: {
    flex: 1.2,
    margin: 1.1,
    backgroundColor: '#FFA500',
    borderRadius: 25,
  },
});
