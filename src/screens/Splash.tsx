import React, {useEffect} from 'react';
import {ImageBackground} from 'react-native';
import {useDispatch} from 'react-redux';
import {clearCart, getList} from '../redux/action/listAction';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {logIn} from '../redux/action/loginAction';
import axios from 'axios';
import { HOME_URL } from '../utils/api';
const Splash = props => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(clearCart());

    checktoken();
  }, []);

  const checktoken = async () => {
    try {
      const url = HOME_URL+'getproductlistall';
      const response = await axios.get(url);
      const list = response.data.map(item => {
        item.quantity = 0;
        return item;
      });
      dispatch(getList(list));
    } catch (err) {}

    const userID = await AsyncStorage.getItem('userID');
    // console.log("userID ko value",userID)
    if (userID) {
      setTimeout(() => {
        dispatch(logIn());
        props.navigation.navigate('HomeStack');
      }, 2000);
    } else {
      setTimeout(() => {
        props.navigation.navigate('WalkThrough');
      }, 2000);
    }
  };
  return (
    <ImageBackground
      style={{flex: 1}}
      source={require('../icons/splash.jpg')}></ImageBackground>
  );
};

export default Splash;
