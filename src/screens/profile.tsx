import React, {useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Alert,
  SafeAreaView,
  AppRegistry,
} from 'react-native';
import Header from '../component/Header';
import {CenterView, Row} from '../component/Widgets';
import Text from '../component/text';
import FastImage from 'react-native-fast-image';
import {useDispatch, useSelector} from 'react-redux';
import {logOut} from '../redux/action/loginAction';
import {rootPush} from '../utils/setRoot';
import {GET_PROFILE, HOME_URL} from '../utils/api';
import {getRequests} from '../network';
import {userID} from '../redux/action/walkthroughAction';
import Background from '../component/background';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {clearCart} from '../redux/action/listAction';

const slides = [
  {
    key: 'k1',
    title: 'Change Password',
    image: require('../icons/pass.png'),
    navigate: 'changePassword',
    // onpress:
  },
  {
    key: 'k2',
    title: 'Payment',
    image: require('../icons/payment.png'),
    navigate: 'payment',
  },
  // {
  //   key: 'k3',
  //   title: 'Info on delivery ',
  //   image: require('../icons/info.png'),
  //   navigate: 'delivery',
  // },
  {
    key: 'k4',
    title: 'Veg / none veg info.',
    image: require('../icons/food.png'),
    navigate: 'vegnon',
  },
  // {
  //   key: 'k6',
  //   title: 'Indrani Basket ON/OFF',
  //   image: require('../icons/food.png'),
  //   navigate: 'indrani',
  // },
  {
    key: 'k5',
    title: 'logout',
    image: require('../icons/logout.png'),
    navigate: 'logout',
  },
];

const Profile = props => {
  const dispatch = useDispatch();
  const [userData, setuserData] = useState([]);
  const [ScheduleData, setScheduleData] = useState([]);

  const [customerCode, setcustomerCode] = useState('');
  useEffect(() => {
    bannerData();
    getProfile();
  }, []);

  const getProfile = async () => {
    const id = await AsyncStorage.getItem('userID');
    const url = GET_PROFILE + id;
    const res = await getRequests(url);
    if (res) {
      setuserData(res);
    } else {
      console.log('error');
    }
  };
  const bannerData = async () => {
    const id = await AsyncStorage.getItem('userID');
    const url = HOME_URL + 'gethomedata/' + id + "/1/0/'";
    const res = await getRequests(url);
    if (res) {
      const datas = res.ScheduleData;
      setScheduleData(datas);
    } else {
      console.log('error');
    }
  };
  const _twoOptionAlertHandler = () => {
    Alert.alert(
      'Logout',

      'Are you sure you want to logout?',
      [
        {
          text: 'Cancel',
        },
        {
          text: 'Logout',
          onPress: () => {
            dispatch(logOut());
            props.navigation.navigate('SignIn');
            dispatch(clearCart());
            AsyncStorage.clear();
          },
        },
      ],
      {cancelable: false},
    );
  };
  const indraniBasket = () => {
    Alert.alert(
      'Indrani Basket',

      'Change Your Basket Mode?',
      [
        {text: 'OFF', onPress: () => console.log('Yes Pressed')},
        {
          text: 'ON',
          onPress: () => console.log('No Pressed'),
          style: 'cancel',
        },
      ],
      {cancelable: false},
    );
  };

  const gotoScreen = item => {
    props.navigation.navigate(item);

    // Navigation.push(props.componentId, {
    //   component: {
    //     name: item,
    //   },
    // });
  };
  return (
    <Background>
      <View
        style={{
          flex: 2,
          elevation: 1,
          borderBottomLeftRadius: 8,
          borderBottomRightRadius: 8,
        }}>
        <Header
          icon={require('../../src/icons/left-back.png')}
          heading={'My Profile'}
          componentId={props.componentId}
          navigation={props.navigation}
        />
        <Row
          innerStyle={{
            padding: 10,
            paddingTop: 20,
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <CenterView
            innerStyle={{
              flex: 0,
              width: 130,
              height: 130,
              borderRadius: 65,
              backgroundColor: '#9AA1B1',
            }}>
            <Text style={{fontSize: 50, fontWeight: '600', color: '#ffffff'}}>
              {' '}
              {userData.customer_code}
            </Text>
          </CenterView>
          <View>
            <Text style={styles.heading}>{userData.customer_name}</Text>

            <Row
              innerStyle={{
                paddingVertical: 6,
              }}>
              <FastImage
                style={styles.iconImage}
                source={require('../icons/info.png')}
                resizeMode={FastImage.resizeMode.contain}
              />

              <Text style={styles.userText}>{userData.resident_name}</Text>
            </Row>
            <Row
              innerStyle={{
                paddingVertical: 6,
              }}>
              <FastImage
                style={styles.iconImage}
                source={require('../icons/block.png')}
                resizeMode={FastImage.resizeMode.contain}
              />

              <Text style={styles.userText}>
                Block No. : {userData.resident_block}(
                {userData.resident_floor_no})
              </Text>
            </Row>
          </View>
        </Row>

        <Row
          innerStyle={{
            paddingVertical: 6,
          }}>
          <FastImage
            style={styles.iconImage}
            source={require('../icons/phone.png')}
            resizeMode={FastImage.resizeMode.contain}
          />
          <Text style={styles.userText}>{userData.contactno}</Text>
        </Row>
        <Row
          innerStyle={{
            paddingVertical: 6,
          }}>
          <FastImage
            style={styles.iconImage}
            source={require('../icons/mess.png')}
            resizeMode={FastImage.resizeMode.contain}
          />
          <Text style={styles.userText}>{userData.email}</Text>
        </Row>
        <Text style={styles.heading}>Delivery Info</Text>
        {ScheduleData.map(item => {
          return (
            <Row
              key={item.weekday}
              innerStyle={{
                paddingVertical: 6,
              }}>
              <FastImage
                style={styles.iconImage}
                source={require('../icons/dTime.png')}
                resizeMode={FastImage.resizeMode.contain}
              />
              <Row>
                <Text style={styles.userText}>{item.WeekdayName}</Text>
                <Text style={styles.userText}>
                  ({item.starttime} - {item.endtime})
                </Text>
              </Row>
            </Row>
          );
        })}
      </View>
      <View style={{height: 15}} />
      <View style={{flex: 1, paddingHorizontal: 15}}>
        {slides.map(data => {
          return (
            <TouchableOpacity
              key={data.key}
              onPress={() => {
                if (data.navigate === 'logout') {
                  _twoOptionAlertHandler();
                } else if (data.navigate == 'indrani') {
                  indraniBasket();
                } else {
                  gotoScreen(data.navigate);
                }
              }}>
              <Row
                key={data.key}
                innerStyle={{
                  paddingVertical: 8,
                  justifyContent: 'space-between',
                }}>
                <Row>
                  <FastImage
                    style={styles.iconImage}
                    source={data.image}
                    resizeMode={FastImage.resizeMode.contain}
                  />
                  <Text style={styles.text}>{data.title}</Text>
                </Row>
                {data.key == 'k5' ? null : (
                  <FastImage
                    style={{width: 15, height: 15}}
                    source={require('../icons/left.png')}
                    resizeMode={FastImage.resizeMode.contain}
                  />
                )}
              </Row>
            </TouchableOpacity>
          );
        })}
      </View>
    </Background>
  );
};
export default Profile;
const styles = StyleSheet.create({
  text: {
    color: '#707070',
    fontSize: 14,
    fontWeight: '800',
    fontFamily: 'Poppins-Regular',
  },
  userText: {
    color: '#707070',
    fontSize: 15,
    fontWeight: '500',
    fontFamily: 'Poppins-Regular',
    marginTop: -2,
  },
  heading: {
    color: '#707070',
    fontSize: 19,
    fontWeight: '300',
    fontFamily: 'Poppins-Regular',
    marginBottom: 2,
  },
  iconImage: {width: 18, height: 18, marginRight: 10},
});
