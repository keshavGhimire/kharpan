import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  SafeAreaView,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import {useSelector} from 'react-redux';
import Background from '../component/background';
import Header from '../component/Header';
import {Row, CenterView} from '../component/Widgets';
import {getRequests} from '../network';
import {PAYMENT_HISTORY} from '../utils/api';
import Icons from 'react-native-vector-icons/FontAwesome5';
import NoData from '../component/noData';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Payment = props => {
  const Id = useSelector(state => state.walkthrough.userID);
  const [Payment, setPayment] = useState([]);
  const [activity, setActivity] = useState(true);
  useEffect(() => {
    paymentData();
  }, []);

  const paymentData = async () => {
    const id = await AsyncStorage.getItem('userID');
    const url = PAYMENT_HISTORY + '/' + Id;
    const res = await getRequests(url);
    if (res) {
      console.log('res', res);
      setPayment(res);

      setActivity(false);
    }
  };
  const renderItem = ({item}) => {
    return (
      <Row
        innerStyle={[
          styles.item,
          {
            backgroundColor:
              item.remarks != 'Order' ? AppSettings.itembackGround : '#ddfcbd',
          },
        ]}>
        <Icons name="file-invoice" size={40} color="#BEBBBB" />
        <View style={{width: '60%'}}>
          <Text style={styles.text}>Date. : {item.date}</Text>
          <Text style={styles.text}>
            {item.remarks == 'Order' ? 'Today Purchase' : 'Paid Amt'}:RS.{' '}
            {item.total_amt === null ? 'NAN' : item.total}
          </Text>
          <Text style={[styles.text, {fontWeight: 'bold'}]}>
            Toatal Due Amt.: {item.Balance}
          </Text>

          <Text numberOfLines={1} style={styles.text}>
            Remarks : {item.remarks}
          </Text>
        </View>
      </Row>
    );
  };
  return (
    <SafeAreaView style={{flex: 1}}>
      {activity ? (
        <CenterView>
          <ActivityIndicator size="large" color="#00ff00" />
        </CenterView>
      ) : (
        <Background>
          <Header
            componentId={props.componentId}
            icon={require('../../src/icons/left-back.png')}
            heading={'Your Payment'}
            navigation={props.navigation}
          />
          <View style={{flex: 1}}>
            {Payment.length > 0 ? (
              <FlatList
                data={Payment}
                renderItem={renderItem}
                keyExtractor={item => item.SN}
              />
            ) : (
              <View style={{marginTop: '30%'}}>
                <NoData
                  heading="NO Payment history"
                  text="You don't have any Payment in your history."
                />
              </View>
            )}
          </View>
        </Background>
      )}
    </SafeAreaView>
  );
};

export default Payment;
const styles = StyleSheet.create({
  item: {
    height: 100,
    width: '99%',
    elevation: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    alignSelf: 'center',
    marginVertical: 8,
  },
  text: {
    color: '#707070',
    fontSize: 14,
    fontWeight: '300',
    fontFamily: 'Poppins-Regular',
  },
});
