import React, {useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Alert,
} from 'react-native';
import Background from '../component/background';
import Header from '../component/Header';
import {CustomButton, DoubleButton} from '../component/button';
import {Row, CenterView} from '../component/Widgets';
import Icon from 'react-native-vector-icons/Entypo';
import AppSettings from '../utils/AppSettings';
import Text from '../component/text';
import Swiper from 'react-native-swiper';
import FastImage from 'react-native-fast-image';
import HTMLView from 'react-native-htmlview';
import {imageUrl, openModel} from '../redux/action/colorAction';
import {useDispatch} from 'react-redux';
const ProductDetails = (route) => {
  const dispatch = useDispatch();
  const [tab, settab] = useState('general_info');
  const [Details, setDetails] = useState([]);
  useEffect(() => {
    setDetails(route.route.params.name);
  }, []);
  return (
    <Background>
      <Header
        icon={require('../../src/icons/left-back.png')}
        heading={'Product Details'}
        cart={true}
        profile={true}
        navigation={route.navigation}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{height: 180, marginBottom: 20}}>
          
          <FastImage
            style={{flex: 1}}
            source={{
              uri: Details?.image_url,
            }}
            resizeMode={FastImage.resizeMode.cover}
          />
        </View>

        <View style={{flex: 1}}>
          <Row
            innerStyle={{
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text style={styles.heading}>{Details?.product_name}</Text>
            <TouchableOpacity
              onPress={() => {
                // dispatch(openModel());
                // dispatch(imageUrl(Details?.product_color_info));
              }}>
              <CenterView innerStyle={styles.outCircle}>
                <View
                  style={[
                    styles.circle,
                    {backgroundColor: Details?.product_color_code},
                  ]}>
                  <Icon name="info-with-circle" size={8} color="#BEBBBB" />
                </View>
              </CenterView>
            </TouchableOpacity>
          </Row>
          <Text style={styles.text}>{Details?.description} </Text>
          <Text
            style={[
              styles.text,
              {
                color: AppSettings.signinbutton,
                fontWeight: '700',
                fontSize: 16,
              },
            ]}>
            {Details?.rate}
          </Text>
          {/* <Text style={styles.text}>
            Additional tax may apply, charged at checkout{' '}
          </Text> */}

          <Row
            innerStyle={{
              justifyContent: 'space-around',
              height: 45,
              backgroundColor: '#fff',
              marginBottom: 8,
            }}>
            <CenterView
              innerStyle={
                tab == 'general_info'
                  ? [
                      styles.tab,
                      {borderBottomWidth: 1.1, borderBottomColor: '#000'},
                    ]
                  : styles.tab
              }>
              <Row>
                <TouchableOpacity onPress={() => settab('general_info')}>
                  <Text
                    style={
                      tab == 'general_info'
                        ? [styles.textTab, {fontSize: 16, fontWeight: '700'}]
                        : styles.textTab
                    }>
                    General info
                  </Text>
                </TouchableOpacity>
              </Row>
            </CenterView>

            <View style={styles.verticalLine} />

            <CenterView
              innerStyle={
                tab == 'storage_info'
                  ? [
                      styles.tab,
                      {borderBottomWidth: 1.1, borderBottomColor: '#000'},
                    ]
                  : styles.tab
              }>
              <TouchableOpacity onPress={() => settab('storage_info')}>
                <Text
                  style={
                    tab == 'storage_info'
                      ? [styles.textTab, {fontSize: 16, fontWeight: '700'}]
                      : styles.textTab
                  }>
                  Storage info
                </Text>
              </TouchableOpacity>
            </CenterView>

            <View style={styles.verticalLine} />

            <CenterView
              innerStyle={
                tab == 'nutrition_info'
                  ? [
                      styles.tab,
                      {borderBottomWidth: 1.1, borderBottomColor: '#000'},
                    ]
                  : styles.tab
              }>
              <TouchableOpacity onPress={() => settab('nutrition_info')}>
                <Text
                  style={
                    tab == 'nutrition_info'
                      ? [styles.textTab, {fontSize: 16, fontWeight: '700'}]
                      : styles.textTab
                  }>
                  Nutrition info{' '}
                </Text>
              </TouchableOpacity>
            </CenterView>
          </Row>
          <View style={{paddingHorizontal: 5}}>
            {tab === 'general_info' ? (
              <HTMLView value={Details?.general_info} />
            ) : tab === 'storage_info' ? (
              <HTMLView value={Details?.storage_info} />
            ) : (
              <HTMLView value={Details?.nutrition_info} />
            )}
          </View>
        </View>
      </ScrollView>
    </Background>
  );
};
ProductDetails.options = {
  bottomTabs: {
    visible: false,
  },
};
export default ProductDetails;
const styles = StyleSheet.create({
  line: {
    height: 0.5,
    width: '100%',
    backgroundColor: '#C1BDBD',
    alignSelf: 'center',
    marginVertical: 8,
  },
  verticalLine: {
    width: 0.5,
    height: '50%',
    backgroundColor: '#C1BDBD',
    alignSelf: 'center',
    marginVertical: 8,
  },
  tab: {
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#C1BDBD',
    backgroundColor: AppSettings.backgroundColor,
  },
  text: {
    fontSize: 13,
    color: AppSettings.SecondaryText,

    paddingBottom: 8,
  },
  textTab: {
    fontSize: 14,
    color: AppSettings.SecondaryText,
  },
  heading: {
    fontSize: AppSettings.maxfontsize,
    color: AppSettings.black,
    fontWeight: '700',
    paddingVertical: 8,
  },
  circle: {
    height: 20,
    width: 20,
    borderRadius: 10,
    alignItems: 'flex-end',
  },
  outCircle: {
    flex: 0,
    height: 30,
    width: 30,
  },
});
