import React, {useState} from 'react';
import {
  View,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {CenterView, Row} from '../component/Widgets';
import FastImage from 'react-native-fast-image';
import {SignInInput} from '../component/inputText';
import {validatePhone, validatePassword} from '../utils/validate';
import {BASE_URL, LOG_IN} from '../utils/api';
import {postTokenlessRequest} from '../network';
import {SnackMsg} from '../utils/SnackMsg';
import Text from '../component/text';
import {useDispatch, useSelector} from 'react-redux';
import {rootPush} from '../utils/setRoot';
import {CustomButton} from '../component/button';
import {HEIGHT, WIDTH} from '../component/metrics';
import AsyncStorage from '@react-native-async-storage/async-storage';
import TokenUpdate from '../function/token';
import {logIn} from '../redux/action/loginAction';
const SignIn = props => {
  const dispatch = useDispatch();
  const [Phone, setPhone] = useState('');
  const [Password, setPassword] = useState('');
  const [loading, setloading] = useState(false);
  // const [Phone, setPhone] = useState('9803495186');
  // const [Password, setPassword] = useState('9803495186');
  // const [Phone, setPhone] = useState('9851032937');
  // const [Password, setPassword] = useState('9841619599');
  // device id
  const signUp = async () => {
    setloading(true);
    if (validatePhone(Phone)) {
      if (validatePassword(Phone)) {
        const url = BASE_URL + LOG_IN;
        const data = {
          password: Password,
          username: Phone,
        };
        const res = await postTokenlessRequest(url, data);
        if (res !== null) {
          // if (res.StatusCode === 200) {
          //   try {
          //     await AsyncStorage.setItem('phone', Phone);
          //     const data = res.userid.toString();
          //     const code = res.Initials;
          //     AsyncStorage.setItem('userID', data);
          //     AsyncStorage.setItem('customerCode', code);
          //     // dispatch(userID(data));
          //     // dispatch(customerCode(code));
          //     dispatch(logIn());
          //     TokenUpdate(data);
          //     console.log('login');

          //     props.navigation.navigate('HomeStack');
          //     setloading(false);
          //   } catch (e) {
          //     setloading(false);
          //   }
          // } else {
          //   SnackMsg(res.message);
          //   setloading(false);

          // }
          try {
            await AsyncStorage.setItem('phone', Phone);
            const data = res.userid.toString();
            const code = res.Initials;
            AsyncStorage.setItem('userID', data);
            AsyncStorage.setItem('customerCode', code);
            dispatch(logIn());
            props.navigation.navigate('HomeStack');

            setloading(false);
          } catch (e) {
            setloading(false);
            console.log(e);
          }
        } else {
          SnackMsg('error has occurred');
          setloading(false);
        }
      } else {
        SnackMsg('enter valid password');
        setloading(false);
      }
    } else {
      SnackMsg('enter valid phone number');
      setloading(false);
    }
  };
  return (
    <ImageBackground
      style={{height: HEIGHT, width: WIDTH}}
      resizeMode="cover"
      source={require('../icons/loginBg.png')}>
      <CenterView innerStyle={{flex: 1}}>
        <FastImage
          resizeMode={FastImage.resizeMode.cover}
          style={{width: 211, height: 256}}
          source={require('../icons/logo.png')}
        />
      </CenterView>
      <View
        style={{
          flex: 1,
          top: -70,
          paddingHorizontal: 40,
        }}>
        <View style={{paddingVertical: 15}}>
          <SignInInput
            placeholder={'USERNAME'}
            keyboardType={'numeric'}
            value={Phone}
            onChangeText={text => setPhone(text)}
          />
          <SignInInput
            placeholder={'PASSWORD'}
            show={true}
            value={Password}
            onChangeText={text => setPassword(text)}
          />
        </View>
        {/* <TouchableOpacity style={styles.forgot}
          onPress={() =>
            rootPush('forgetPassword')}>
          <Text style={styles.forgetText}>Forget Passwords ?</Text>
        </TouchableOpacity> */}
        <View style={{paddingVertical: 15}}>
          <CustomButton
            onPress={() => signUp()}
            loading={loading}
            style={{alignSelf: 'center', width: '100%'}}>
            login Now
          </CustomButton>
          <CustomButton
            onPress={() => rootPush('signUp')}
            style={{
              alignSelf: 'center',
              width: '100%',
              marginTop: 25,
              backgroundColor: 'red',
            }}>
            NEW USER
          </CustomButton>
        </View>
      </View>
    </ImageBackground>
  );
};
export default SignIn;
const styles = StyleSheet.create({
  btn: {
    width: '100%',
    height: 45,
    borderRadius: 5,
    backgroundColor: '#2CCB64',
    marginTop: 40,
  },
  text: {
    fontSize: 16,
    fontFamily: 'Poppins-Regular',
    color: '#9597A8',
  },
  heading: {
    fontSize: 25,
    fontFamily: 'Poppins-Regular',
    fontWeight: 'bold',
    textAlign: 'center',
    paddingTop: 5,
  },
  forgot: {
    alignSelf: 'flex-end',
  },
  forgetText: {
    fontSize: 18,
  },
});
