import React, { useState } from 'react'
import { View, ImageBackground, StyleSheet } from 'react-native';
import FastImage from 'react-native-fast-image';
import { CustomButton } from '../component/button';
import Text from '../component/text';
import { SignInInput } from '../component/inputText';
import { HEIGHT, WIDTH } from '../component/metrics';
import { CenterView } from '../component/Widgets';
import { Navigation } from 'react-native-navigation';
import { rootPush } from '../utils/setRoot';

export default function forgetPassword(props) {
    const [Phone, setPhone] = useState('');
    const [loading, setloading] = useState(false);
    const forgetSubmit = async () => {
        setloading(true);
        if (validatePhone(Phone)) {
            const url = FORGETPASSWORD;
            const data = {
                contactnumber: Phone,
            };
            const res = await postTokenlessRequest(url, data);
            if (res !== null) {
                if (res.StatusCode === 200) {
                    setloading(false);
                    dispatch(openModel());
                }
            }
        } else {
            SnackMsg('enter valid phone number');
            setloading(false);
        }
    };
    goBack = () => {
        Navigation.pop(props.componentId);
    };
    return (
        <ImageBackground
            style={{ height: HEIGHT, width: WIDTH, resizeMode: 'center' }}
            source={require('../icons/loginBg.png')}>
            <CenterView innerStyle={{ flex: 1 }}>
                <FastImage
                    resizeMode={FastImage.resizeMode.cover}
                    style={{ width: 211, height: 256 }}
                    source={require('../icons/logo.png')}
                />
            </CenterView>
            <View style={styles.body}>
                <SignInInput
                    placeholder={'Username'}
                    keyboardType={'numeric'}
                    value={Phone}
                    onChangeText={(text) => setPhone(text)}
                />
                <Text style={styles.forgetText}>
                    Please wait up to 3 minutes to receive{"\n"}
                    reset link.</Text>
                <CustomButton
                    onPress={() => forgetSubmit()}
                    loading={loading}
                    style={{ alignSelf: 'center', width: '100%', marginTop: 50 }}>
                    Reset Password
                </CustomButton>
                <CustomButton
                    onPress={() => rootPush('signIn')}

                    style={{ alignSelf: 'center', width: '100%', marginTop: 10, backgroundColor: "red" }}>
                    Cancel
                </CustomButton>
            </View >
        </ImageBackground>
    )
}
const styles = StyleSheet.create({
    body: {
        flex: 1,
        marginHorizontal: 35,
    },
    forgetText: {
        marginTop: 25,
        textAlign: "center"

    }
});
