import React, {useState, useEffect} from 'react';
import {View, FlatList, RefreshControl, SafeAreaView} from 'react-native';
import HistoryCart from '../../component/historyCart';
import NoData from '../../component/noData';
import Header from '../../component/Header';
import {ORDER_URL} from '../../utils/api';
import {getRequests} from '../../network';
import {useSelector} from 'react-redux';
import {CenterView} from '../../component/Widgets';
import {ActivityIndicator} from 'react-native-paper';

export default Delivered = (props) => {
  const [OrderList, setOrderList] = useState([]);
  const Id = useSelector((state) => state.walkthrough.userID);
  const [refreshing, setRefreshing] = useState(true);
  const [activity, setActivity] = useState(true);
  useEffect(() => {
    OrderListData();
  }, []);

  const OrderListData = async () => {
    const Id = await AsyncStorage.getItem('userID');

    const url = ORDER_URL + '3/' + Id;
    const res = await getRequests(url);

    if (res) {
      setOrderList(res);
      setActivity(false);
      setRefreshing(false);
    } else {
      console.log('error');
    }
  };
  const _renderItem = ({item}) => {
    return <HistoryCart componentId={props.componentId} item={item}     navigation={props.navigation}
    />;
  };
  onRefresh = async () => {
    setRefreshing(true);
    OrderListData();
  };
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fafafa'}}>
      {activity ? (
        <CenterView>
          <ActivityIndicator size="small" color="#00ff00" />
        </CenterView>
      ) : (
        <View style={{flex: 1, marginHorizontal: 20}}>
          {OrderList.length > 0 ? (
            <FlatList
              data={OrderList}
              renderItem={_renderItem}
              keyExtractor={(item) => item.masterid.toString()}
              showsVerticalScrollIndicator={false}
              refreshControl={
                <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
              }
            />
          ) : (
            <View style={{marginTop: '30%'}}>
              <NoData
                heading="NO ORDERS"
                text="You don't have any orders in your history."
              />
            </View>
          )}
        </View>
      )}
    </SafeAreaView>
  );
};
