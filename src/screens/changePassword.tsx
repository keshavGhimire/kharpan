import React, {useState, useEffect} from 'react';
import {StyleSheet, View} from 'react-native';
import Background from '../component/background';
import Text from '../component/text';
import AppSettings from '../utils/AppSettings';
import {CustomButton} from '../component/button';
import {CustomInput} from '../component/inputText';
import Header from '../component/Header';
import {validatePassword} from '../utils/validate';
import {SnackMsg} from '../utils/SnackMsg';
import {postTokenlessRequest} from '../network';
import {CHNAGE_PASSWORD} from '../utils/api';
import {logOut} from '../redux/action/loginAction';
import {rootPush} from '../utils/setRoot';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
const ChangePasswordMain = props => {
  useEffect(() => {}, []);
  const dispatch = useDispatch();
  const [OldPassword, setOldPassword] = useState('');
  const [NewPassword, setNewPassword] = useState('');
  const [ConfirmPassword, setConfirmPassword] = useState('');
  const [loading, setloading] = useState(false);

  const changePassword = async () => {
    const Id = await AsyncStorage.getItem('userID');

    setloading(true);
    if (
      validatePassword(OldPassword) &&
      validatePassword(NewPassword) &&
      validatePassword(ConfirmPassword)
    ) {
      if (NewPassword === ConfirmPassword) {
        const url = CHNAGE_PASSWORD;
        const data = {
          UserId: Id,
          OldPassword: OldPassword,
          NewPassword: NewPassword,
          ConfirmPassword: ConfirmPassword,
        };

        const res = await postTokenlessRequest(url, data);
        if (res !== null) {
          if (res.StatusCode === 200) {
            dispatch(logOut());
            props.navigation.navigate('SignIn');
          } else {
            SnackMsg(res.message);
            setloading(false);
          }
        } else {
          SnackMsg("Password doesn't match");
          setloading(false);
        }
      } else {
        SnackMsg("Password and Confirm Password  doesn't match");
        setloading(false);
      }
    } else {
      SnackMsg('Password should be more then 7 character');
      setloading(false);
    }
  };
  return (
    <Background style={{backgroundColor: '#ffffff'}}>
      <View style={{flex: 5}}>
        <Header
          componentId={props.componentId}
          icon={require('../../src/icons/left-back.png')}
          heading={'Change Password'}
          navigation={props.navigation}
        />
        <Text style={styles.heading}>Wait ! Just a second.</Text>
        {/* <Text
          style={[
            styles.label,
            {fontWeight: '100', margin: 0, marginVertical: 20},
          ]}>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard.
        </Text> */}
        <View style={{marginVertical: 20}} />

        <CustomInput
          image={require('../../src/icons/password.png')}
          placeholder={'Your old password.'}
          show={true}
          value={OldPassword}
          onChangeText={text => setOldPassword(text)}
        />
        <CustomInput
          image={require('../../src/icons/password.png')}
          placeholder={'New password'}
          show={true}
          value={NewPassword}
          onChangeText={text => setNewPassword(text)}
        />
        <CustomInput
          image={require('../../src/icons/password.png')}
          placeholder={'Confirm new password'}
          show={true}
          value={ConfirmPassword}
          onChangeText={text => setConfirmPassword(text)}
        />
      </View>
      <View style={{flex: 1, alignItems: 'center'}}>
        <CustomButton loading={loading} onPress={() => changePassword()}>
          Save and proceed.
        </CustomButton>
      </View>
    </Background>
  );
};

const styles = StyleSheet.create({
  heading: {
    fontSize: 24,
    marginTop: '10%',
    fontWeight: '700',
  },
});

export default ChangePasswordMain;
