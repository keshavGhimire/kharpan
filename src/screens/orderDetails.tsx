import React, {useEffect, useState} from 'react';
import {StyleSheet, View, FlatList, ActivityIndicator} from 'react-native';

import Header from '../component/Header';

import OrderCard from '../component/card';
import {ORDER_DETAILS} from '../utils/api';
import {getRequests} from '../network';
import NoData from '../component/noData';
import {CenterView} from '../component/Widgets';
import Background from '../component/background';
import AppSettings from '../utils/AppSettings';
const OrderDetails = (props) => {
  const [OrderDetails, setOrderDetails] = useState([]);
  const [activity, setActivity] = useState(true);
  useEffect(() => {
    orderDetailsData();
  }, []);

  const orderDetailsData = async () => {
    const url = ORDER_DETAILS + props.route.params.name;
    const res = await getRequests(url);
    if (res) {
      setOrderDetails(res);
      setActivity(false);
    } else {
      console.log('err');
    }
  };
  return (
    <Background>
      {activity ? (
        <CenterView>
          <ActivityIndicator size="small" color="#00ff00" />
        </CenterView>
      ) : (
        <View style={{flex: 1}}>
          <Header
            componentId={props.componentId}
            icon={require('../../src/icons/left-back.png')}
            heading={'Order HistoryKeshs'}
            profile={true}
            navigation={props.navigation}
          />

          <View style={{flex: 1}}>
            <View style={{flex: 1}}>
              {OrderDetails.length > 0 ? (
                <View>
                  <FlatList
                    data={OrderDetails}
                    showsVerticalScrollIndicator={false}
                    renderItem={({item}) => {
                      return (
                        <OrderCard
                          componentId={props.componentId}
                          item={item}
                        />
                      );
                    }}
                    keyExtractor={(item) => item.productid.toString()}
                  />
                </View>
              ) : (
                <View style={{marginTop: '30%'}}>
                  <NoData
                    heading="NO ORDERS"
                    text="You don't have any orders in your history."
                  />
                </View>
              )}
            </View>
          </View>
        </View>
      )}
    </Background>
  );
};

export default OrderDetails;
const styles = StyleSheet.create({
  tab: {
    borderBottomWidth: 1,
    borderColor: '#C1BDBD',
    backgroundColor: AppSettings.backgroundColor,
  },
  text: {
    fontSize: 13,
    color: AppSettings.SecondaryText,

    paddingBottom: 8,
  },
  textTab: {
    fontSize: 14,
    color: AppSettings.SecondaryText,
  },
  verticalLine: {
    width: 0.5,
    height: '50%',
    backgroundColor: '#C1BDBD',
    alignSelf: 'center',
    marginVertical: 8,
  },
});
