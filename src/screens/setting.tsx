import React from 'react';
import Background from '../component/background';
import Header from '../component/Header';
import Text from '../component/text';
import {Switch} from 'react-native-paper';
import {Row} from '../component/Widgets';
import {useDispatch, useSelector} from 'react-redux';
import {
  notificationOff,
  notificationOn,
} from '../redux/action/notificationAction';

const Setting = (props) => {
  const survayers = useSelector((state) => state.notification.notification);
  const dispatch = useDispatch();
  const handleButtonPress = () => {
    // survayers ? dispatch(notificationOff()) : dispatch(notificationOn());
  };

  return (
    <Background>
      <Header
        componentId={props.componentId}
        icon={require('../../src/icons/left-back.png')}
        heading={'Setting'}
        navigation={props.navigation}
      />
      <Row
        innerStyle={{
          height: 50,
          width: '98%',
          justifyContent: 'space-between',
          alignItems: 'center',
          backgroundColor: '#fff',
          alignSelf: 'center',
          elevation: 2,
          borderRadius: 8,
          paddingHorizontal: 5,
        }}>
        <Text style={{fontSize: 18}}>Notification</Text>
        <Switch value={false} onValueChange={handleButtonPress} />
      </Row>
    </Background>
  );
};

export default Setting;
