import React, {useState} from 'react';
import {StyleSheet, View, TextInput, Text} from 'react-native';
import Header from '../component/Header';
import Background from '../component/background';
import FastImage from 'react-native-fast-image';
import AppSettings from '../utils/AppSettings';
const Search = (props) => {
  const [searchKey, setsearchKey] = useState('');
  return (
    <Background>
      <Header
        componentId={props.componentId}
        icon={require('../icons/Menu.png')}
        heading={'Kharpan'}
        profile={true}
        sidemenu={true}
      />

      <View style={styles.search}>
        <FastImage
          style={{width: 20, height: 20}}
          source={require('../icons/search.png')}
        />
        <TextInput
          placeholder="Search for ......"
          placeholderTextColor={'#A6BCD0'}
          style={styles.textinput}
          onChangeText={(text) => setsearchKey(text)}
          value={searchKey}
        />
      </View>
      <View style={{alignItems: 'center'}}>
        <FastImage
          resizeMode={FastImage.resizeMode.center}
          style={{width: 200, height: 200, margintop: 50}}
          source={require('../icons/error.png')}
        />
        <Text style={styles.text}>No search result found!</Text>
      </View>
    </Background>
  );
};

export default Search;
const styles = StyleSheet.create({
  search: {
    height: 50,
    alignItems: 'center',
    borderRadius: 5,
    flexDirection: 'row',
    marginVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: '#ffff',
    elevation: 0.5,
  },
  textinput: {
    color: '#fafafa',
    paddingLeft: 20,

    fontWeight: '400',
  },
  text: {
    color: AppSettings.primaryfontColor,
    fontSize: AppSettings.primaryfontsize,
    fontWeight: '600',
  },
});
