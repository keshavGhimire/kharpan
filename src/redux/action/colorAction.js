export function openModel() {
    return function (dispatch) {
      dispatch({type: 'SHOW'});
    };
  }
  
  export function closeModel() {
    return function (dispatch) {
      dispatch({type: 'HIDE'});
    };
  }
  export function imageUrl(item) {
    return function (dispatch) {
      dispatch({type: 'URL', payload: item});
    };
  }