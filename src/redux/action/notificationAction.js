export function notificationOn() {
    return function (dispatch) {
      dispatch({type: 'ON'});
    };
  }
  
  export function notificationOff() {
    return function (dispatch) {
      dispatch({type: 'OFF'});
    };
  }
export function notificationSet() {
    return function (dispatch) {
      dispatch({type: 'SET'});
    };
  }
  
  export function notificationNotSet() {
    return function (dispatch) {
      dispatch({type: 'NOTSET'});
    };
  }
  export function remainingDays(item) {
    return function (dispatch) {
      dispatch({type: 'REMAIN', payload: item});
    };
  }