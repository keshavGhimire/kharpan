export function addToCart(item) {
  return function (dispatch) {
    dispatch({type: 'ADD_TO_CART', payload: item});
  };
}

export function removeFromCart(id) {
  return function (dispatch) {
    dispatch({type: 'REMOVE_FROM_CART', payload: id});
  };
}

export function updateCart(id, item) {
  return function (dispatch) {
    dispatch({type: 'UPDATE_CART', id: id, payload: item});
  };
}
