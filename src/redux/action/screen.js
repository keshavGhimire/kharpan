export function ScreenAction(page) {
  return function (dispatch) {
    dispatch({type: 'PAGE', payload: page});
  };
}
