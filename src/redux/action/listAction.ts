import axios from 'axios';
import {SnackMsg} from '../../utils/SnackMsg';

export function getList(item) {
  return {
    type: 'LOAD_LIST',
    payload: item,
  };
}

export function incrementList(id) {
  return {
    type: 'INCREMENT',
    payload: id,
  };
}

export function decrementList(id) {
  return {
    type: 'DECREMENT',
    payload: id,
  };
}
export function searchItem(name) {
  return {
    type: 'SEARCH',
    payload: name,
  };
}

export function loadDefault() {
  return {
    type: 'LOAD_DEFAULT',
  };
}

export function filter(name) {
  return {
    type: 'FILTER',
    payload: name,
  };
}

export function removeFromCart(id) {
  return {
    type: 'REMOVE_CART',
    payload: id,
  };
}
export function clearCart() {
  return {
    type: 'CLEARCART',
  };
}
