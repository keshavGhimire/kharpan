const initialState = {
    model: false,
    url:""
  };
  
  export function colorReducers(state = initialState, action) {
    switch (action.type) {
      case 'SHOW':
        return {
          ...state,
          model: true,
        };
      case 'HIDE':
        return {
          ...state,
          model: false,
        };
      case 'URL':
        return {
          ...state,
          url: action.payload
        };
    
      default:
        return state;
    }
  }
  
  