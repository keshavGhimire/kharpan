import { combineReducers } from 'redux';
import { walkthroughReducers } from './walkthroughReducers';
import { loginReducers } from './loginReducers';
import { cartReducers } from './cartReducers';
import { kharpanReducers } from './kharpanReducers';
import { ScreenReducers } from './screenReducer';
import { dayReducers } from './dayReducers';
import { notificationReducers } from './notificationReducers';
import { colorReducers } from './colorReducers';

const reducers = combineReducers({
  walkthrough: walkthroughReducers,
  log: loginReducers,
  cart: cartReducers,
  kharpan: kharpanReducers,
  screen: ScreenReducers,
  day: dayReducers,
  notification: notificationReducers,
  color: colorReducers,
});

export default reducers;
