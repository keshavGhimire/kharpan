const initialState = {
    list: [],
};

export function dayReducers(state = initialState, action) {
    switch (action.type) {
        case 'ADD':
            return {
                ...state,
                list:  action.payload,
            };
        default:
            return state;
    }
}
