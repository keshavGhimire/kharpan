const initialState = {
  page: 'initial',
};

export function ScreenReducers(state = initialState, action) {
  switch (action.type) {
    case 'PAGE':
      return {
        ...state,
        page: action.payload,
      };

    default:
      return state;
  }
}
