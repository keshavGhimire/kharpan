const initialState = {
  lists: [],
  duplicateList: [],
  cartList: [],
};

export function kharpanReducers(state = initialState, action) {
  switch (action.type) {
    case 'LOAD_LIST':
      return {
        ...state,
        lists: action.payload,
        duplicateList: action.payload,
      };
    case 'INCREMENT':
      let newItem = {};
      let newCartList = [];
      const newList = state.lists.map(item => {
        if (item.id == action.payload) {
          item.quantity = item.quantity + parseFloat(item.qty_interval);
          newItem = item;
        }
        return item;
      });
      const newCartItem = state.cartList.find(it => it.id == action.payload);
      if (newCartItem == undefined) {
        newCartList = [...state.cartList, newItem];
      } else {
        newCartList = state.cartList.map(i => {
          if (i.id == action.payload) {
            i = newItem;
          }
          return i;
        });
      }
      return {
        ...state,
        lists: newList,
        cartList: newCartList,
      };
    case 'DECREMENT':
      let decrementedItem = {};
      let decrementedCartList = [];
      const decrementedList = state.lists.map(item => {
        if (item.id == action.payload) {
          if (item.quantity > 0.001) {
            item.quantity =
              parseFloat(item.quantity).toFixed(3) -
              parseFloat(item.qty_interval).toFixed(3);
            if (item.quantity < 0) {
              item.quantity = 0;
            }

            decrementedItem = item;
          }
        }
        return item;
      });

      const newdCartItem = state.cartList.find(i => i.id == action.payload);
      if (newdCartItem != undefined) {
        if (newdCartItem.quantity == 0) {
          decrementedCartList = state.cartList.filter(
            item => item.id !== action.payload,
          );
        } else {
          decrementedCartList = state.cartList.map(item => {
            if (item.id == action.payload) {
              item = decrementedItem;
            }
            return item;
          });
        }
      }

      return {
        ...state,
        lists: decrementedList,
        cartList: decrementedCartList,
      };

    case 'SEARCH':
      const searchedList = state.duplicateList.filter(item =>
        item.product_name.toLowerCase().includes(action.payload.toLowerCase()),
      );
      return {
        ...state,
        lists: searchedList,
      };
    case 'LOAD_DEFAULT':
      return {
        ...state,
        lists: state.duplicateList,
      };
    case 'FILTER':
      const filteredList = state.duplicateList.filter(
        item => item.category_name == action.payload,
      );
      return {
        ...state,
        lists: filteredList,
      };
    case 'CLEARCART':
      return {
        ...state,
        cartList: [],
      };
    case 'REMOVE_CART':
      const deletedCart = state.cartList.filter(
        item => item.id !== action.payload,
      );
      const updatedList = state.lists.map(item => {
        if (item.id == action.payload) {
          item.quantity = 0;
        }
        return item;
      });
      return {
        ...state,
        cartList: deletedCart,
        lists: updatedList,
      };

    default:
      return state;
  }
}
