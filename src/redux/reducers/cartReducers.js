const initialState = {
  list: [],
};

export function cartReducers(state = initialState, action) {
  switch (action.type) {
    case 'ADD_TO_CART':
      return {
        ...state,
        list: [...state.list, action.payload],
      };
    case 'REMOVE_FROM_CART':
      const removedCart = state.list.filter(
        (item) => item.id !== action.payload,
      );
      return {
        ...state,
        list: removedCart,
      };
    case 'UPDATE_CART':
      const updatedCart = state.list.map((item) => {
        if (item.id === action.id) {
          item = action.payload;
        }
        return item;
      });
      return {
        ...state,
        list: updatedCart,
      };
    default:
      return state;
  }
}
