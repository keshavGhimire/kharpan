const initialState = {
  success: false,
  userID: 0,
  customerCode: '',
 
};

export function loginReducers(state = initialState, action) {
  switch (action.type) {
    case 'TRUE':
      return {
        ...state,
        success: true,
      };

    case 'FALSE':
      return {
        ...state,
        success: false,
      };
      case 'USERID':
      return {
        ...state,
        userID: action.payload
      };
    case 'CODE':
     
      return {
        ...state,
        customerCode: action.payload
      };
    
    default:
      return state;
  }
}
