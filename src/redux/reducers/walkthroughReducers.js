const initialState = {
  success: false,
  vegnon: true,
  userID: 0,
  customerCode: '',
};

export function walkthroughReducers(state = initialState, action) {
  switch (action.type) {
    case 'SHOW':
      return {
        ...state,
        success: false,
      };

    case 'HIDE':
      return {
        ...state,
        success: true,
      };
    case 'OPEN':
      return {
        ...state,
        vegnon: true,
      };
    case 'CLOSE':
      return {
        ...state,
        vegnon: false,
      };
    

    default:
      return state;
  }
}

