const initialState = {
    notification: true,
    scheduled:false,
    remain:''
  };
  
  export function notificationReducers(state = initialState, action) {
    switch (action.type) {
      case 'ON':
        return {
          ...state,
          notification: true,
        };
  
      case 'OFF':
        return {
          ...state,
          notification: false,
        };
      case 'SET':
        return {
          ...state,
          scheduled: true,
        };
  
      case 'NOTSET':
        return {
          ...state,
          scheduled: false,
        };
        case 'REMAIN':
          return {
            remain:'',
            ...state,
            remain: action.payload,
          };
      default:
        return state;
    }
  }
  