import PushNotification from 'react-native-push-notification';
import moment from 'moment'



export const FirebaseNotification = (data) => {

  PushNotification.localNotification({
    autoCancel: true,
    bigText:
      data.notification.body,
    subText: 'Kharpan',
    title: data.notification.title,
    message: 'Expand me to see more',
    vibrate: true,
    vibration: 300,
    playSound: true,
    soundName: 'default',
    // actions: '["Yes", "No"]',
  });
};

export const scheduledNotification = () => {
 
  const date = new Date()
  const hour = 20 - date.getHours()
  if (hour > 1) {
    PushNotification.localNotificationSchedule({
      //... You can use all the options from localNotifications
      message: "Tommarow there will be delivery in you'r laocation so please place an order if any !!! ", // (required)
      date:new Date().setHour(20), // 8 pm
      allowWhileIdle: false, // (optional) set notification to work while on doze, default: false
      repeatType: 'week',
    })
  
  } else {
    PushNotification.localNotificationSchedule({
      //... You can use all the options from localNotifications
      message: "Tommarow there will be delivery in you'r laocation so please place an order if any !!! ", // (required)
      date: new Date(Date.now() + 300 * 1000), // in 60 secs
      allowWhileIdle: false, // (optional) set notification to work while on doze, default: false
      repeatType: 'week',
    })
  }
  console.log(hour)
  
};

