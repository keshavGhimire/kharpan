import {
  NavigationContainer,
  NavigationContainerRef,
  ParamListBase,
  useNavigationContainerRef,
} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Cart from './src/screens/cart';
import Splash from './src/screens/Splash';
import WalkThrough from './src/screens/WalkThrough';
import SignIn from './src/screens/signIn';
import SignUp from './src/screens/signUp';
import Landing from './src/screens/Landing';
import SideMenu from './src/utils/sideMenu';
import Order from './src/screens/order';
import Contact from './src/screens/contact';
import Setting from './src/screens/setting';
import Profile from './src/screens/profile';
import ChangePasswordMain from './src/screens/changePassword';
import Payment from './src/screens/payment';
import VegNonVeg from './src/screens/vegNonveg';
import ProductDetails from './src/screens/productDetails';
import OrderDetails from './src/screens/orderDetails';
import Rejected from './src/screens/Order/Rejected';

const Stack = createNativeStackNavigator();
const Drawer = createDrawerNavigator();

function DrawerNavigation({
  navigation,
}: {
  navigation: NavigationContainerRef<ParamListBase>;
}) {
  return (
    <Drawer.Navigator
      initialRouteName={'home'}
      drawerContent={() => <SideMenu navigation={navigation} />}
      screenOptions={{
        headerShown: false,
      }}>
      <Drawer.Screen name={'home'} component={Landing} options={{}} />
      <Stack.Screen name={'myOrder'} component={Order} />
      <Stack.Screen name={'contactUs'} component={Contact} />
      <Stack.Screen name={'setting'} component={Setting} />
      <Stack.Screen name={'profile'} component={Profile} />
    </Drawer.Navigator>
  );
}

export default function App() {
  const navigationRef = useNavigationContainerRef();

  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator
        initialRouteName={'Splash'}
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name={'Splash'} component={Splash} />
        <Stack.Screen name={'WalkThrough'} component={WalkThrough} />
        <Stack.Screen name={'SignIn'} component={SignIn} />
        <Stack.Screen name={'SignUp'} component={SignUp} />
        <Stack.Screen name={'HomeStack'} component={DrawerNavigation} />
        <Stack.Screen name={'cart'} component={Cart} />
        <Stack.Screen name={'changePassword'} component={ChangePasswordMain} />
        <Stack.Screen name={'payment'} component={Payment} />
        <Stack.Screen name={'vegnon'} component={VegNonVeg} />
        <Stack.Screen name={'productDetails'} component={ProductDetails} />
        <Stack.Screen name={'rejected'} component={Rejected} />
        <Stack.Screen name={'orderDetails'} component={OrderDetails} />

      </Stack.Navigator>
    </NavigationContainer>
  );
}
